﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapService
{
    public class Country
    {
        private Dictionary<string, string> iso2ByFullNameEs;
        private Dictionary<string, string> iso2ByFullNameEn;
        private Dictionary<string, string> iso2ByISO3;
        private Dictionary<string, string> iso2ByISO2;
        private Dictionary<string, string> languagesByISO2;
        private Dictionary<string, string> iso2BySpecialCases;
        private Dictionary<string, string> fnsCountryCodes;
        private List<List<string>> countryData;

        public Country()
        {
            this.countryData = new List<List<string>>();
            this.countryData = Global.mysqlConnection.getInfoCountry();
            this.iso2ByFullNameEn = new Dictionary<string, string>();
            this.iso2ByFullNameEs = new Dictionary<string, string>();
            this.iso2ByISO3 = new Dictionary<string, string>();
            this.iso2ByISO2 = new Dictionary<string, string>();
            this.languagesByISO2 = new Dictionary<string, string>();
            this.fnsCountryCodes = new Dictionary<string, string>();

            for (int i = 0; i < countryData.LongCount(); i++)
            {
                this.iso2ByFullNameEs.Add(Global.quitAccents(countryData[i][0]).ToUpper(), countryData[i][3]);

                this.iso2ByFullNameEn.Add(Global.quitAccents(countryData[i][2]).ToUpper(), countryData[i][3]);

                this.iso2ByISO2.Add(countryData[i][3].ToUpper(), countryData[i][3]);

                this.iso2ByISO3.Add(countryData[i][1].ToUpper(), countryData[i][3]);

                this.languagesByISO2.Add(countryData[i][3].ToUpper(), countryData[i][6]);

                if (countryData[i][7] != "0")
                {
                    this.fnsCountryCodes.Add(countryData[i][7], countryData[i][3]);
                }

                setIso2BySpecialCases();
            }
        }

        public Dictionary<string, string> Iso2ByFullNameEs
        {
            get { return this.iso2ByFullNameEs; }
        }

        public Dictionary<string, string> Iso2ByFullNameEn
        {
            get { return this.iso2ByFullNameEn; }
        }

        public Dictionary<string, string> Iso2ByISO3
        {
            get { return this.iso2ByISO3; }
        }

        public Dictionary<string, string> Iso2ByISO2
        {
            get { return this.iso2ByISO2; }
        }

        public Dictionary<string, string> LanguagesByISO2
        {
            get { return this.languagesByISO2; }
        }

        public void setIso2BySpecialCases()
        {
            // special cases
            iso2BySpecialCases = new Dictionary<string, string>();

            //United Kingdom
            this.iso2BySpecialCases.Add(Global.quitAccents("ing").ToUpper(), "GB");
            //Portugal
            this.iso2BySpecialCases.Add(Global.quitAccents("portug").ToUpper(), "PT");
            //Dominican Republic
            this.iso2BySpecialCases.Add(Global.quitAccents("R.Dominicana").ToUpper(), "DO");
            //Spain
            this.iso2BySpecialCases.Add(Global.quitAccents("Espa").ToUpper(), "ES");
            //Chile
            this.iso2BySpecialCases.Add(Global.quitAccents("CHI").ToUpper(), "CL");
            //USA
            this.iso2BySpecialCases.Add(Global.quitAccents("EE.UU.").ToUpper(), "US");

        }

        public Dictionary<string, string> Iso2BySpecialCases
        {
            get { return this.iso2BySpecialCases; }
        }

        public Dictionary<string, string> FnsCountryCodes
        {
            get { return this.fnsCountryCodes; }
        }

    }
}