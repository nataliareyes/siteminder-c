﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapService
{
    public class Product
    {
        private bool iconciergePrellegada;
        bool iconcierge;
        bool fidelityOnSite;
        bool fidelity;
        bool reviewExpress;

        public Product()
        {

        }
        public bool IconciergePrellegada
        {
            get { return this.iconciergePrellegada; }
            set { this.iconciergePrellegada = value; }
        }

        public bool Iconcierge
        {
            get { return this.iconcierge; }
            set { this.iconcierge = value; }
        }

        public bool FidelityOnSite
        {
            get { return this.fidelityOnSite; }
            set { this.fidelityOnSite = value; }
        }

        public bool Fidelity
        {
            get { return this.fidelity; }
            set { this.fidelity = value; }
        }

        public bool ReviewExpress
        {
            set { this.reviewExpress = value; }
            get { return this.reviewExpress; }
        }



    }
}