﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapService
{
    public class STRData
    {
        private DateTime date;
        private int roomsAvailable;
        private int roomsSold;
        private decimal roomsRevenue;
        private string currency;

        private int idHotel;

        public STRData(int idHotel, int roomsAvailable, int roomsSold, decimal roomsRevenue, DateTime date, string currency)
        {
            this.idHotel = idHotel;
            this.date = date;
            this.roomsAvailable = roomsAvailable;
            this.roomsSold = roomsSold;
            this.roomsRevenue = roomsRevenue;
            this.currency = currency;
        }

        public int IdHotel
        {
            get { return this.idHotel; }
            set { this.idHotel = value; }
        }

        public DateTime Date
        {
            get { return this.date; }
            set { this.date = value; }
        }

        public int RoomsAvailable
        {
            get { return this.roomsAvailable; }
            set { this.roomsAvailable = value; }
        }

        public int RoomsSold
        {
            get { return this.roomsSold; }
            set { this.roomsSold = value; }
        }

        public decimal RoomsRevenue
        {
            get { return this.roomsRevenue; }
            set { this.roomsRevenue = value; }
        }

        public string Currency
        {
            get { return this.currency; }
            set { this.currency = value; }
        }
    }
}