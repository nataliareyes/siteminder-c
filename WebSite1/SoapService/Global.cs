﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SoapService
{
    public class Global
    {
        public static DBConnection devConnection = new DBConnection("db-dev.myhotel.io", "myhotel_production", "myhotel_dev", "mYH0telDEV");
        public static DBConnection mysqlConnection = new DBConnection("production.cluster-cw0wpp4r1lg1.us-west-1.rds.amazonaws.com", "myhotel_production", "myhotel", "mYH0tel#7102!Aur0ra");
        public static string myHotelFtp = "ftp://ftp.myhotel.com.es";

        public static int MaxReportsPerDay = 5;

        public static int MinuteMillisecond = 1000 * 60;

        public static int CheckinMaxDifference = 5; //represents that if a guest is found by algorithms same as other guest
                                                    //but their checkin differs in 5 days or more, it's considered as a new guest.

        public static int DaysAheadFetchStandard = 7; //represents the amount of days ahead that a hotel will fetch information from their future guests

        public enum ErrorTypes { None, NoNewReports, ConnectionError, NoNewGuests, myHotelIntegratorError, Nofound };

        public enum gmailIntegrationsReports { email, password, port, hostname };
        public enum fileType { excel2007, excel2003, txt, csv };

        public enum flagGetListGuest { ListToInsert, ListToUpdate };

        public static int timeWaitIntegration = 90;

        public static int IntegrationStartMinutes = 60;

        public static int daysBeforeGuestData = 2;

        public static string[] formats = { "dd-MM-yyyy","MM-dd-yyyy", "dd-MM-yy", "yyyyMMdd","ddMMyyyy", "yyyy-MM-dd", "MM-dd-yy","dd-MMM-yy","dd-MMM-yy HH:mm","dd-MM-yy HH:mm","dd-MMM-yy HH:mm:ss tt","dd-MMM-yy hh:mm:ss tt",
                                            "dd-MM-yyyy hh:mm:ss", "MM-dd-yyyy hh:mm:ss", "dd-MM-yy hh:mm:ss", "yyyyMMdd hh:mm:ss","ddMMyyyy hh:mm:ss", "yyyy-MM-dd hh:mm:ss",
                                            "dd-MM-yyyy hh:mm:ss tt","MM-dd-yyyy hh:mm:ss tt", "dd-MM-yy hh:mm:ss tt", "yyyyMMdd hh:mm:ss tt","ddMMyyyy hh:mm:ss tt", "yyyy-MM-dd hh:mm:ss tt",
                                            "dd-MM-yyyy hh:mm","MM-dd-yy hh:mm", "dd-MM-yy hh:mm", "yyyyMMdd hh:mm","ddMMyyyy hh:mm", "yyyy-MM-dd hh:mm",
                                            "dd-MM-yyyy h:mm:ss","MM-dd-yy hh:mm:ss", "dd-MM-yy h:mm:ss", "yyyyMMdd h:mm:ss","ddMMyyyy h:mm:ss", "yyyy-MM-dd h:mm:ss",
                                            "dd-mmm-yyyy h:mm:ss", "mmm-dd h:mm:ss","dd-mmm h:mm:ss", "dd-mmm-yy h:mm:ss", "yyyymmmdd h:mm:ss","ddmmmyyyy h:mm:ss", "yyyy-mmm-dd h:mm:ss",
                                            "dd-mmm-yyyy hh:mm:ss","dd-mmm hh:mm:ss", "dd-mmm-yy hh:mm:ss", "yyyymmmdd hh:mm:ss","ddmmmyyyy hh:mm:ss", "yyyy-mmm-dd hh:mm:ss",
                                            "dd/MM/yyyy hh:mm:ss", "dd/MM/yy hh:mm:ss", "yyyy/MM/dd hh:mm:ss","dd-mmm-yy","dd-mmm-yyyy",
                                            "dd/MM/yyyy hh:mm:ss tt", "dd/MM/yy hh:mm:ss tt", "yyyy/MM/dd hh:mm:ss tt","MM/dd/yyyy hh:mm:ss tt","MM/dd/yyyy HH:mm:ss tt",
                                            "dd/MM/yyyy h:mm:ss", "MM/dd/yyyy h:mm:ss","dd/MM/yy h:mm:ss", "yyyy/MM/dd h:mm:ss","MM/dd/yyyy h:mm:ss tt",
                                            "dd/MM/yyyy h:mm", "dd/MM/yy h:mm", "yyyy/MM/dd h:mm",
                                            "dd/MM/yyyy", "dd/MM/yy", "yyyy/MM/dd","MM/dd/yyyy","dd/mmm/yyyy","dd/mmm/yy"};

        public static Dictionary<ErrorTypes, string> ErrorMessages = new Dictionary<ErrorTypes, string>() {
            { ErrorTypes.None, "OK" },
            { ErrorTypes.NoNewReports, "NO_RECORD"},
            { ErrorTypes.ConnectionError, "NO_CONNECTED"},
            { ErrorTypes.NoNewGuests, "NO_NEW_GUEST"},
            { ErrorTypes.myHotelIntegratorError, "ERROR_INTEGRATION"},
            { ErrorTypes.Nofound, "XX"} //Country Code not found = XX
        };

        public static Dictionary<ErrorTypes, bool> IntegrationError = new Dictionary<ErrorTypes, bool>() {
            { ErrorTypes.None, false},
            { ErrorTypes.NoNewGuests, false},
            { ErrorTypes.NoNewReports, true},
            { ErrorTypes.ConnectionError, true},
            { ErrorTypes.myHotelIntegratorError, true}
        };

        public static Dictionary<gmailIntegrationsReports, string> integrationsReportsEmail = new Dictionary<gmailIntegrationsReports, string>() {
            { gmailIntegrationsReports.email, "integrationsreports@gmail.com" },
            { gmailIntegrationsReports.hostname, "pop.gmail.com"},
            { gmailIntegrationsReports.password, "HotelMy.2018"},
            { gmailIntegrationsReports.port, "993"},
        };

        public static Dictionary<fileType, string> extensionTypeFile = new Dictionary<fileType, string>() {
            { fileType.excel2007, ".xlsx" },
            { fileType.excel2003, ".xls" },
            { fileType.txt, ".txt"},
            { fileType.csv, ".csv"},
        };

        public static string quitAccents(string inputString)
        {
            Regex a = new Regex("[á|à|ä|â]", RegexOptions.Compiled);
            Regex e = new Regex("[é|è|ë|ê]", RegexOptions.Compiled);
            Regex i = new Regex("[í|ì|ï|î]", RegexOptions.Compiled);
            Regex o = new Regex("[ó|ò|ö|ô]", RegexOptions.Compiled);
            Regex u = new Regex("[ú|ù|ü|û]", RegexOptions.Compiled);
            Regex n = new Regex("[ñ|Ñ]", RegexOptions.Compiled);
            inputString = a.Replace(inputString, "a");
            inputString = e.Replace(inputString, "e");
            inputString = i.Replace(inputString, "i");
            inputString = o.Replace(inputString, "o");
            inputString = u.Replace(inputString, "u");
            inputString = n.Replace(inputString, "n");
            return inputString;
        }

        // conectamos al servidor

        public static Country countries = new Country();

        public enum CountryCodeType { ISO2, ISO3, FullNameEs, FullNameEn, FNSCode };

        public enum IntegrationType { Manual, Reporting, ExternalConnection, EmailReader };

    }
}