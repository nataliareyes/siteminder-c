﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System.Net;

namespace SoapService
{
    public class Service1 : IReservationService
    {
        string strUrl = "http://app.myhotel.com.es/fidelity/status/check_hotel_str.php?idHotel=";
        public Creds credHeader;

        List<Guest> guests = new List<Guest>();
        List<Hotel> hotels = new List<Hotel>();
        Dictionary<Guest, Hotel> guestListToInsert;
        Dictionary<Guest, Hotel> guestListToUpdate;
        List<List<string>> hotelData;

        public string AddHotelReservation(OTA_HotelResNotifRequest request)
        {
            try
            {
                string response = "Hola";
                int idHotel = request.PropertyID;
                hotelData = Global.mysqlConnection.GetDataHotel(idHotel); //This is a Test...

                bool isHotelSTR = getHotelType(idHotel);

                switch (isHotelSTR)
                {
                    case true:
                        GetSTRDataFromRequest(request);
                        break;

                    case false:
                        GetHotelDataFromRequest(request);
                        break;
                    default:
                        break;
                }
                /*
               hotels.Add(new ClassLibrary.Global_Class.HotelReservation(Global.IntegrationType.Reporting, idHotel, "SiteminderUp", DateTime.Now));

               foreach (Hotel hotel in hotels)
               {
                   hotel.LastModifiedDate = DateTime.Now.AddDays(-1);
                   if (hotel.CheckIfIntegration())
                   {
                       //Aqui recupero los datos de siteminder guests = hotel.DoIntegration();
                       response = hotel.HotelName;
                   }
               }*/

                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns
        /// </summary>
        /// <param name="idHotel"></param>
        /// <returns></returns>
        public bool getHotelType(int idHotel)
        {
            try
            {
                bool isSTR = false;
                string text = string.Empty;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strUrl + "idHotel=" + idHotel);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    text = reader.ReadToEnd();
                }

                string result = text;


                if (result.Equals("Active"))
                {
                    isSTR = true;
                }


                return isSTR;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        //its like a "GetGuestFromFiles" from integrator master.
        public bool GetSTRDataFromRequest(OTA_HotelResNotifRequest request)
        {
            // En este metodo, se guardan todos los datos que estamos recibiendo desde el OTA_HotelResNotifRequest request que nos hicieron
            // Para despues guardar o actualizar en bdd segun corresponda, y aplicando los filtros correspondientes.
            /*
            try
            {
               
            }
            catch (Exception)
            {

                throw;
            }*/
            return false;
        }

        public bool GetHotelDataFromRequest(OTA_HotelResNotifRequest request)
        {
            // En este metodo, se guardan todos los datos que estamos recibiendo desde el OTA_HotelResNotifRequest request que nos hicieron
            // Para despues guardar o actualizar en bdd segun corresponda, y aplicando los filtros correspondientes.
            return true;
        }

        public string GetData(int value)
        {

            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public bool Login(string user, string pass)
        {
            string usr = "naty";
            string pss = "123";

            if (usr.Equals(user) && pss.Equals(pass))
            {
                return true;
            }
            else return false;
        }


        public string XMLData(string id)
        {
            //this was a test.
            throw new NotImplementedException();
        }
    }
}
