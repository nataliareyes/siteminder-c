﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapService
{
    public class IntegrationProfile
    {
        private int checkOutdaysBefore;
        private int daysBeforeGuestData;
        private DateTime lastFetchedDateLog;
        private int countIntegrationLogToday;
        private List<List<string>> listDateLogToday;
        private List<List<string>> listHotelProfile;
        int repeatingRequest;
        int timeAfterRepeating;

        public IntegrationProfile(int idHotel)
        {
            InitializeData(idHotel);
        }

        public void InitializeData(int idHotel)
        {
            DaysBeforeGuestData = getDaysBeforeGuestDataByHotel(idHotel);
            CheckOutdaysBefore = DaysBeforeGuestData + Global.daysBeforeGuestData;
            listDateLogToday = getListDateLogTodayByHotel(idHotel);
            lastFetchedDateLog = getFetchedDateLogByHotel(listDateLogToday);
            countIntegrationLogToday = getCountIntegrationLogByHotel(listDateLogToday);
            listHotelProfile = getHotelProfile(idHotel);
            repeatingRequest = getRepeatingRequest(listHotelProfile);
            timeAfterRepeating = getTimeAfterRepeating(listHotelProfile);
        }

        public int getDaysBeforeGuestDataByHotel(int idHotel)
        {
            int daysBefore = 0;
            try
            {
                List<List<string>> Lastregisteredguest = Global.mysqlConnection.GetLastRecordByHotel(idHotel);

                if (Lastregisteredguest.Count > 0)
                {
                    string createdDate = Lastregisteredguest[0][4];
                    DateTime createdDateDT;

                    if (DateTime.TryParse(createdDate, out createdDateDT))
                    {
                        // Difference in days, hours, and minutes.
                        System.TimeSpan ts = DateTime.Now - createdDateDT;

                        // Difference in days.
                        daysBefore = ts.Days;

                    }
                }
                else daysBefore = 30; //standard cuando no tiene ningún huésped histórico
            }
            catch (Exception)
            {
                throw;
            }
            return daysBefore;
        }

        public List<List<string>> getHotelProfile(int idHotel)
        {
            return Global.mysqlConnection.getHotelProfileByHotel(idHotel);
        }

        public List<List<string>> getListDateLogTodayByHotel(int idHotel)
        {
            return Global.mysqlConnection.getListDateLogTodayByHotel(idHotel);

        }

        public int getRepeatingRequest(List<List<string>> listHotelProfile)
        {
            try
            {
                if (listHotelProfile.Count > 0)
                {

                    return Int32.Parse(listHotelProfile[0][4]);
                }
                else
                {
                    return 20;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int getTimeAfterRepeating(List<List<string>> listHotelProfile)
        {
            try
            {
                if (listHotelProfile.Count > 0)
                {
                    string dateStr = listHotelProfile[0][5];
                    DateTime timeAfterRepeating;

                    if (DateTime.TryParse(dateStr, out timeAfterRepeating))
                    {
                        return timeAfterRepeating.Hour;

                    }
                    else
                    {
                        return 8;
                    }
                }
                else
                {
                    return 8;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int getCountIntegrationLogByHotel(List<List<string>> listDateLogToday)
        {
            return listDateLogToday.Count;
        }
        public DateTime getFetchedDateLogByHotel(List<List<string>> listDateLogToday)
        {
            //DateTime lastFetchedDate;
            try
            {
                DateTime lastFetchedDateLog;
                if (listDateLogToday.Count > 0)
                {
                    string createdDate = listDateLogToday[0][1];


                    if (DateTime.TryParse(createdDate, out lastFetchedDateLog))
                    {
                        return lastFetchedDateLog;
                    }
                    else
                    {
                        return DateTime.Now.AddDays(-1);
                    }
                }
                else
                {
                    return DateTime.Now.AddDays(-1);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public int CheckOutdaysBefore
        {
            get { return this.checkOutdaysBefore; }
            set { this.checkOutdaysBefore = value; }
        }

        public int DaysBeforeGuestData
        {
            get { return this.daysBeforeGuestData; }
            set { this.daysBeforeGuestData = value; }
        }

        public DateTime LastFetchedDateLog
        {
            get { return this.lastFetchedDateLog; }
            set { this.lastFetchedDateLog = value; }
        }

        public int CountIntegrationLogToday
        {
            get { return this.countIntegrationLogToday; }
            set { this.countIntegrationLogToday = value; }
        }

        public int RepeatingRequest
        {
            get { return this.repeatingRequest; }
            set { this.repeatingRequest = value; }
        }

        public int TimeAfterRepeating
        {
            get { return this.timeAfterRepeating; }
            set { this.timeAfterRepeating = value; }
        }


    }
}