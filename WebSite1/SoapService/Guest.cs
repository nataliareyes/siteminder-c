﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapService
{
    public class Guest
    {
        private string name;
        private string lastName;
        private string email;
        private string roomNumber;
        private string phoneNumber;
        private int language;
        private DateTime checkin;
        private DateTime checkout;
        private string countryCode;

        private string gender;
        private string channel;
        private DateTime birthDate;

        private string reservationId;
        private string profileId;

        private int idHotel;

        private int id;
        private string flagEmailValido;

        public Guest(string name, string lastName, string email, string roomNumber,
            DateTime checkin, DateTime checkout, int language, string phoneNumber,
            int idHotel, string countryCode, string gender = "", string channel = "", DateTime birthDate = new DateTime(),
            string reservationId = "", string profileId = "")
        {
            this.name = name;
            this.lastName = lastName;
            this.email = email;
            this.roomNumber = roomNumber;
            this.checkin = checkin;
            this.checkout = checkout;
            this.language = language;
            this.phoneNumber = phoneNumber;
            this.idHotel = idHotel;
            this.countryCode = countryCode;
            this.id = -1;
            this.gender = gender;
            this.channel = channel;
            this.birthDate = birthDate;
            this.reservationId = reservationId;
            this.profileId = profileId;
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string LastName
        {
            get { return this.lastName; }
            set { this.lastName = value; }
        }

        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }

        public string RoomNumber
        {
            get { return this.roomNumber; }
            set { this.roomNumber = value; }
        }

        public DateTime Checkin
        {
            get { return this.checkin; }
            set { this.checkin = value; }
        }

        public DateTime Checkout
        {
            get { return this.checkout; }
            set { this.checkout = value; }
        }

        public int Language
        {
            get { return this.language; }
            set { this.language = value; }
        }

        public int Id
        {
            set { this.id = value; }
            get { return this.id; }
        }

        public int IdHotel
        {
            get { return this.idHotel; }
            set { this.idHotel = value; }
        }
        public string CountryCode
        {
            get { return this.countryCode; }
            set { this.countryCode = value; }
        }

        public string PhoneNumber
        {
            get { return this.phoneNumber; }
            set { this.phoneNumber = value; }
        }

        public string Gender
        {
            get { return this.gender; }
            set { this.gender = value; }
        }

        public string Channel
        {
            get { return this.channel; }
            set { this.channel = value; }
        }

        public DateTime BirthDate
        {
            get { return this.birthDate; }
            set { this.birthDate = value; }
        }

        public string ReservationId
        {
            get { return this.reservationId; }
            set { this.reservationId = value; }
        }

        public string ProfileId
        {
            get { return this.profileId; }
            set { this.profileId = value; }
        }

        public string FlagEmailValido
        {
            get { return this.flagEmailValido; }
            set { this.flagEmailValido = value; }
        }


    }
}