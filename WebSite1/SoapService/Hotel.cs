﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using System.Text.RegularExpressions;

namespace SoapService
{
    public abstract class Hotel
    {

        private string hotelName;
        private int idHotel;

        private string checkinTime;
        private string checkoutTime;
        private int flagCliente;

        private string lastModifiedFileName;

        private DateTime lastModifiedDate;

        private string PMSName;
        public IntegrationProfile integrationPolitics;
        private bool conectionError;
        private string messageException;

        public Product products;

        public bool flagGuest = false;
        int filesCount = 0;

        //Log4Net
        public ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Hotel(Global.IntegrationType typeIntegration, int idHotel, string PMSName)
        {
            this.idHotel = idHotel;
            this.PMSName = PMSName;
            this.conectionError = false;
            this.products = new Product();
            List<List<string>> hotelData;

            hotelData = Global.mysqlConnection.GetDataHotel(idHotel);

            if (hotelData != null)
            {
                this.hotelName = hotelData[0][0];
                this.checkinTime = hotelData[0][1];
                this.checkoutTime = hotelData[0][2];
                this.flagCliente = Int32.Parse(hotelData[0][5]);

                this.products.IconciergePrellegada = Convert.ToBoolean(Int32.Parse(hotelData[0][3]));
                this.products.Iconcierge = Convert.ToBoolean(Int32.Parse(hotelData[0][4]));
                this.products.FidelityOnSite = Convert.ToBoolean(Int32.Parse(hotelData[0][6]));
                this.products.Fidelity = Convert.ToBoolean(Int32.Parse(hotelData[0][7]));
                this.products.ReviewExpress = Convert.ToBoolean(Int32.Parse(hotelData[0][8]));
            }

            this.lastModifiedFileName = idHotel + "_LastModified.xml";

            if (System.IO.File.Exists(lastModifiedFileName))
            {
                this.lastModifiedDate = DeserializeLastModified();
            }
            else
            {
                this.lastModifiedDate = DateTime.Now.AddDays(-Global.daysBeforeGuestData);
                if (SerializeLastModified(this.lastModifiedDate))
                    WriteLog("Last modified report date saved SUCCESSFULL");
            }
        }

        public abstract List<Guest> DoIntegration();

        protected virtual List<Guest> FilterCheckoutCheckin(List<Guest> allGuests)
        {
            List<Guest> guestsFilter = new List<Guest>();

            foreach (Guest guest in allGuests)
            {
                if (products.IconciergePrellegada)
                {
                    if (guest.Checkin.Date <= DateTime.Now.Date.AddDays(Global.CheckinMaxDifference))
                    {
                        guestsFilter.Add(guest);
                    }
                }
                else if (products.FidelityOnSite || products.Iconcierge)
                {
                    if (guest.Checkin.Date <= DateTime.Now.Date && guest.Checkout.Date >= DateTime.Now.Date.AddDays(-integrationPolitics.CheckOutdaysBefore))
                    {
                        guestsFilter.Add(guest);
                    }
                }
                else if (products.Fidelity || products.ReviewExpress)
                {
                    if (guest.Checkout.Date >= DateTime.Now.Date.AddDays(-integrationPolitics.CheckOutdaysBefore - 1) &&
                        guest.Checkout <= DateTime.Now.AddDays(Global.DaysAheadFetchStandard))
                    {
                        guestsFilter.Add(guest);
                    }
                }
                else
                {
                    WriteLog("***XXX   IT DOESN'T HAVE ANY ASSIGNED PRODUCTS  XXXX***");
                }
            }

            return guestsFilter;
        }

        protected void EmailFilter(List<Guest> guests)
        {
            foreach (Guest guest in guests)
            {
                if (!Regex.IsMatch(guest.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                {
                    guest.Email = "-";
                }
                else if (guest.Email.ToLowerInvariant().Contains("notiene") || guest.Email.ToLowerInvariant().Contains("nodeja") || guest.Email.ToLowerInvariant().Contains("recepcion") || guest.Email.ToLowerInvariant().Contains("desk") ||
                    guest.Email.ToLowerInvariant().Contains("notiene") || guest.Email.ToLowerInvariant().Contains("info") || guest.Email.ToLowerInvariant().Contains("contact"))
                {
                    guest.Email = "-";
                }
            }
        }

        protected List<Guest> FilterGuests(List<Guest> allGuests)
        {
            List<Guest> filteredGuests = new List<Guest>();

            //remove all duplicate guests
            foreach (Guest guest in allGuests)
            {
                int removed = filteredGuests.RemoveAll(x => x.Name == guest.Name && x.LastName == guest.LastName && x.Email == guest.Email);

                filteredGuests.Add(guest);
            }

            //contains a number regex
            Regex dontContainNumbers = new Regex(@"^(?=.*\p{N})");

            //these are all the hotels that are allowed numbers in their guests' names and last names
            List<int> idHotelsWithNumbers = new List<int> { 273, 277, 286, 299, 300, 301, 309, 387, 476, 477, 569, 570, 571, 572, 573, 574, 575, 576, 577, 579, 920, 687 };

            //remove all invalid name / lastname formats (containing a number)
            int removedInvalidNames = filteredGuests.RemoveAll(x => (dontContainNumbers.Match(x.Name).Success || dontContainNumbers.Match(x.LastName).Success) &&
                !idHotelsWithNumbers.Contains(x.IdHotel));

            //remove all invalid name / lastname formats (internal Hotel guests)
            removedInvalidNames = filteredGuests.RemoveAll(x => x.Name.ToLowerInvariant().Contains("hotel") || x.LastName.ToLowerInvariant().Contains("hotel") ||
                                    x.Name.ToLowerInvariant().Contains("public") || x.LastName.ToLowerInvariant().Contains("public"));


            return filteredGuests;
        }

        protected string RemoveSpaces(string text)
        {
            if (text != null)
            {
                if (text.EndsWith(" ")) return RemoveSpaces(text.Substring(0, text.Length - 1));
                else if (text.StartsWith(" ")) return RemoveSpaces(text.Substring(1));
                else return text;
            }
            else
            {
                return "";
            }
        }

        protected bool isValidNameGuest(Guest guest)
        {
            //these are all the hotels that are allowed numbers in their guests' names and last names
            List<int> idHotelsWithNumbers = new List<int> { 273, 277, 286, 299, 300, 301, 309, 387, 476, 477, 569, 570, 571, 572, 573, 574, 575, 576, 577, 579, 920 };

            //regex that contains numbers in the name (generally internal codes)
            if ((Regex.IsMatch(guest.Name, @"^(?=.*\p{N})") || Regex.IsMatch(guest.LastName, @"^(?=.*\p{N})")) &&
                !idHotelsWithNumbers.Contains(guest.IdHotel))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void WriteLog(string log)
        {
            logger.Debug("[idH:" + idHotel + "] [" + HotelName + "] - " + log);
            Console.WriteLine(DateTime.Now.ToString() + " -[idH:" + idHotel + "] [" + HotelName + "] - " + log);
        }

        protected abstract DateTime GetDateTimeFromString(string date);

        protected abstract string[] GetFormatDate();

        public string HotelName
        {
            get { return this.hotelName; }
            set { this.hotelName = value; }
        }

        public int IdHotel
        {
            get { return this.idHotel; }
            set { this.idHotel = value; }
        }

        public string CheckinTime
        {
            get { return this.checkinTime; }
            set { this.checkinTime = value; }
        }

        public string CheckoutTime
        {
            get { return this.checkoutTime; }
            set { this.checkoutTime = value; }
        }

        public Boolean ConectionError
        {
            get { return this.conectionError; }
            set { this.conectionError = value; }
        }

        public string MessageException
        {
            get { return this.messageException; }
            set { this.messageException = value; }
        }


        public DateTime LastModifiedDate
        {
            get { return this.lastModifiedDate; }
            set { this.lastModifiedDate = value; }
        }

        public string LastModifiedFileName
        {
            get { return this.lastModifiedFileName; }
            set { this.lastModifiedFileName = value; }
        }

        public string PmsName
        {
            get { return this.PMSName; }
            set { this.PMSName = value; }
        }

        public int FlagCliente
        {
            get { return this.flagCliente; }
            set { this.flagCliente = value; }
        }

        protected bool SerializeLastModified(DateTime lastModified)
        {
            try
            {
                System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(DateTime));

                System.IO.StreamWriter file = new System.IO.StreamWriter(lastModifiedFileName);
                writer.Serialize(file, lastModified);
                file.Close();

                return true;
            }
            catch (Exception e)
            {
                logger.Error(e.Message + "\nSource: " + e.Source + "\nStackTrace: " + e.StackTrace);
                Console.WriteLine(DateTime.Now.ToString() + " - "
                    + HotelName + " - " + e.Message + "\nSource: " + e.Source + "\nStackTrace: " + e.StackTrace);
                return false;
            }
        }

        protected DateTime DeserializeLastModified()
        {
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(DateTime));

                System.IO.StreamReader reader = new System.IO.StreamReader(lastModifiedFileName);

                DateTime lastModified = (DateTime)serializer.Deserialize(reader);

                reader.Close();

                return lastModified;
            }
            catch (Exception e)
            {
                logger.Error(e.Message + "\nSource: " + e.Source + "\nStackTrace: " + e.StackTrace);
                Console.WriteLine(DateTime.Now.ToString() + " - "
                    + HotelName + " - " + e.Message + "\nSource: " + e.Source + "\nStackTrace: " + e.StackTrace);
                return DateTime.MinValue;
            }
        }

        public Dictionary<String, DateTime> GetGuestMinAndMaxCheckinRange(List<Guest> guests)
        {
            Dictionary<String, DateTime> dateRange = new Dictionary<String, DateTime>();
            var minDateCheckin = guests.Min(date => date.Checkin);
            var maxDateCheckin = guests.Max(date => date.Checkin);
            dateRange.Add("MinDateCheckin", minDateCheckin);
            dateRange.Add("MaxDateCheckin", maxDateCheckin);

            Console.WriteLine("minDateCheckin " + minDateCheckin);
            Console.WriteLine("maxDateCheckin " + maxDateCheckin);
            return dateRange;
        }

        public List<Guest> GetGuestByRangeDate(int idHotel, DateTime minDateCheckin, DateTime maxDateCheckin)
        {
            List<List<string>> guestsWithFilter;
            List<Guest> guests = new List<Guest>();
            guestsWithFilter = Global.mysqlConnection.CheckGuestsByDateRange(idHotel, minDateCheckin, maxDateCheckin);
            guests = ChangeListGuestTypeStringToTypeGuest(guestsWithFilter);
            return guests;
        }

        public List<Guest> ChangeListGuestTypeStringToTypeGuest(List<List<string>> guestsWithFilter)
        {
            /*
             * 
             *  0           nombre, 
             *  1           apellido,
             *  2           email, 
             *   3          habitacion, 
             *   4          checkin, 
             *   5          checkout, 
             *   6          idioma_f, 
             *   7          telefono, 
             *   8          id_hotel, 
             *   9          countryCode, 
             *   10         id, 
             *   11         flagEmailValido, 
             *   12         genero, 
             *   13         fecha_nacimiento, 
             *   14         idReserva, 
             *   15         idPerfil 
             */

            List<Guest> guests = new List<Guest>();

            foreach (List<string> subListGuestWithFilter in guestsWithFilter)
            {
                String name = subListGuestWithFilter[0];
                String lastName = subListGuestWithFilter[1];
                String email = subListGuestWithFilter[2];
                String roomNumber = subListGuestWithFilter[3];
                DateTime checkin = DateTime.Parse(subListGuestWithFilter[4]);
                DateTime checkout = DateTime.Parse(subListGuestWithFilter[5]);
                int language = Int32.Parse(subListGuestWithFilter[6]);
                String phoneNumber = subListGuestWithFilter[7];
                int idHotel = Int32.Parse(subListGuestWithFilter[8]);
                String countryCode = subListGuestWithFilter[9];
                int id = Convert.ToInt32(subListGuestWithFilter[10]);
                string flagEmailValido = subListGuestWithFilter[11];
                string gender = subListGuestWithFilter[12];
                string fecha_nacimiento = subListGuestWithFilter[13];
                string idReserva = subListGuestWithFilter[14];
                string idPerfil = subListGuestWithFilter[15];

                Guest guest = new Guest(name, lastName, email, roomNumber, checkin, checkout, language, phoneNumber, idHotel, countryCode);
                guest.Id = id; //el id es muy importante para el update
                guest.FlagEmailValido = flagEmailValido; //guardamos el flagEmailValido que venia
                guest.Gender = gender;
                guest.ReservationId = idReserva;
                guest.ProfileId = idPerfil;

                if (!string.IsNullOrEmpty(fecha_nacimiento))
                {
                    try
                    {
                        DateTime birthDate = DateTime.Parse(fecha_nacimiento);
                        guest.BirthDate = birthDate;
                    }
                    catch { }
                }

                guests.Add(guest);

            }
            return guests;
        }

        public Dictionary<Guest, Hotel> GetGuestListToInsertOrUpdate(List<Guest> guestsPms, List<Guest> guestsDDBB, Global.flagGetListGuest flagGetListGuests)
        {
            Dictionary<Guest, Hotel> guestsToInsert = new Dictionary<Guest, Hotel>();
            Dictionary<Guest, Hotel> guestsToUpdate = new Dictionary<Guest, Hotel>();
            Dictionary<Guest, Hotel> guestsExisting = new Dictionary<Guest, Hotel>();
            Dictionary<Guest, Hotel> listGuestToReturn = new Dictionary<Guest, Hotel>();

            foreach (Guest guestPms in guestsPms)
            {
                List<Guest> guestsDataBase = new List<Guest>();
                guestsDataBase = guestsDDBB.FindAll(x => (x.Name == guestPms.Name) && (x.LastName == guestPms.LastName) && (x.Checkin >= guestPms.Checkin.AddDays(-3) && x.Checkin <= guestPms.Checkin.AddDays(3)));

                if (guestsDataBase.Count > 0)
                {
                    foreach (Guest guestDataBase in guestsDataBase)
                    {
                        bool flagChangeEmail = false;
                        string emailPmsAux = "";
                        string emailDataBaseAux = "";
                        string emailSelected = "-";
                        string phoneNumberAux = "";
                        string roomNumberAux = "";

                        bool flagMayusGuestPms = guestPms.Email.Any(c => char.IsUpper(c)); //Valida si existen Mayusculas en el email que viene desde el PMS
                        bool flagMayusGuestDataBase = guestDataBase.Email.Any(c => char.IsUpper(c)); //Valida si existen Mayusculas en el email que viene DDBB

                        emailPmsAux = flagMayusGuestPms ? emailPmsAux = guestPms.Email.ToLower() : guestPms.Email; //Cambios a minusculas
                        emailDataBaseAux = flagMayusGuestDataBase ? emailDataBaseAux = guestDataBase.Email.ToLower() : guestDataBase.Email;  //Cambios a minusculas

                        if (emailPmsAux != emailDataBaseAux)
                        {
                            if (ValidateEmail(emailPmsAux) && ValidateEmail(emailDataBaseAux))
                            {
                                emailSelected = emailPmsAux;
                                flagChangeEmail = true;
                            }
                            else if (ValidateEmail(emailPmsAux) && !ValidateEmail(emailDataBaseAux))
                            {
                                emailSelected = emailPmsAux;
                                flagChangeEmail = true;
                            }
                            else if (!ValidateEmail(emailPmsAux) && !ValidateEmail(emailDataBaseAux))
                            {
                                if (emailPmsAux != "-")
                                {
                                    emailSelected = "-";
                                    flagChangeEmail = true;
                                }
                                else
                                {
                                    flagChangeEmail = false;
                                }
                            }
                            else
                            {
                                emailSelected = emailDataBaseAux;
                                flagChangeEmail = false;
                            }

                        }
                        else
                        {
                            if (emailPmsAux.Equals(emailDataBaseAux) && ValidateEmail(emailPmsAux) && ValidateEmail(emailDataBaseAux))
                            {
                                emailSelected = emailDataBaseAux;
                                flagChangeEmail = false;
                            }
                        }

                        if (flagChangeEmail == true ||
                            guestPms.Checkin.DayOfYear != guestDataBase.Checkin.DayOfYear ||
                            guestPms.Checkout.DayOfYear != guestDataBase.Checkout.DayOfYear ||
                            guestPms.RoomNumber != guestDataBase.RoomNumber ||
                            guestPms.Gender != guestDataBase.Gender ||
                            guestPms.BirthDate != guestDataBase.BirthDate ||
                            guestPms.ReservationId != guestDataBase.ReservationId ||
                            guestPms.ProfileId != guestDataBase.ProfileId)
                        {
                            roomNumberAux = guestPms.RoomNumber;

                            string flagEmailValido = guestDataBase.FlagEmailValido;//para hacer update del flagEmailValido si es que cargaron uno nuevo
                            if (guestPms.Email != guestDataBase.Email && guestPms.Email.Contains("@") && guestDataBase.Email.Contains("@"))
                            {
                                flagEmailValido = "NULL";
                            }

                            if (String.IsNullOrEmpty(guestPms.PhoneNumber))
                            {
                                phoneNumberAux = !String.IsNullOrEmpty(guestDataBase.PhoneNumber) ? guestDataBase.PhoneNumber : "";
                            }

                            Guest guest = new Guest(guestPms.Name, guestPms.LastName, emailSelected, roomNumberAux, guestPms.Checkin, guestPms.Checkout, guestPms.Language, phoneNumberAux, guestPms.IdHotel, guestPms.CountryCode);
                            guest.Id = guestDataBase.Id; //el id es necesario para el update
                            guest.FlagEmailValido = flagEmailValido; //update del flagEmailValido si es que viene diferente
                            guest.Gender = guestPms.Gender;
                            guest.BirthDate = guestPms.BirthDate;
                            guest.ReservationId = guestPms.ReservationId;
                            guest.ProfileId = guestPms.ProfileId;

                            guestsToUpdate.Add(guest, this);
                        }
                        else
                        {
                            guestsExisting.Add(new Guest(guestPms.Name, guestPms.LastName, guestPms.Email, guestPms.RoomNumber, guestPms.Checkin, guestPms.Checkout, guestPms.Language, guestPms.PhoneNumber, guestPms.IdHotel, guestPms.CountryCode, guestPms.Gender,
                                guestPms.Channel, guestPms.BirthDate, guestPms.ReservationId, guestPms.ProfileId), this);
                        }
                    }
                }
                else
                {
                    guestsToInsert.Add(new Guest(guestPms.Name, guestPms.LastName, guestPms.Email, guestPms.RoomNumber, guestPms.Checkin, guestPms.Checkout, guestPms.Language, guestPms.PhoneNumber, guestPms.IdHotel, guestPms.CountryCode, guestPms.Gender,
                                guestPms.Channel, guestPms.BirthDate, guestPms.ReservationId, guestPms.ProfileId), this);
                }
            }

            switch (flagGetListGuests)
            {
                case Global.flagGetListGuest.ListToInsert:
                    listGuestToReturn = guestsToInsert;
                    break;
                case Global.flagGetListGuest.ListToUpdate:

                    listGuestToReturn = guestsToUpdate;
                    break;
                default:
                    Console.WriteLine("Error");
                    break;
            }

            return listGuestToReturn;
        }

        public int InsertGuests(Dictionary<Guest, Hotel> guests)
        {
            try
            {
                return Global.mysqlConnection.InsertBulkGuests(guests);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int UpdateGuests(Dictionary<Guest, Hotel> guests)
        {
            try
            {
                return Global.mysqlConnection.UpdateGuestsBulk(guests);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static void ConvertToDateTime(string value)
        {
            DateTime convertedDate;
            try
            {
                convertedDate = Convert.ToDateTime(value);
                Console.WriteLine("'{0}' converts to {1} {2} time.",
                                  value, convertedDate,
                                  convertedDate.Kind.ToString());
            }
            catch (FormatException)
            {
                Console.WriteLine("'{0}' is not in the proper format.", value);
            }
        }

        public int[] InsertUpdateCount(List<Guest> guests)
        {
            WriteLog("Updating Database...");
            WriteLog("Total Guests: " + guests.Count);
            try
            {
                int insertCount = 0;
                int updateCount = 0;
                int sameCount = 0;

                Dictionary<Guest, Hotel> guestsToInsert = new Dictionary<Guest, Hotel>();
                Dictionary<Guest, Hotel> guestsToUpdate = new Dictionary<Guest, Hotel>();

                foreach (Guest guest in guests)
                {
                    int id = Global.mysqlConnection.CheckGuestExists(guest);

                    if (id == 0)
                    {
                        //int rows = Global.MySqlQueries.insertedGuests(guest,this);
                        guestsToInsert.Add(guest, this);

                        WriteLog("EMAIL DEL INSERTADO, PRODUCCION " + guest.Email);
                        insertCount++;
                    }
                    else if (id != -1) //si no hubo error
                    {
                        List<List<string>> existingGuest = Global.mysqlConnection.GetGuestById(id);
                        DateTime existingCheckin = new DateTime();
                        DateTime existingCheckout = new DateTime();
                        if (DateTime.TryParse(existingGuest[0][0], out existingCheckin) &&
                            DateTime.TryParse(existingGuest[0][1], out existingCheckout)
                                 && existingGuest[0][0] != "" && existingGuest[0][1] != "")
                        {

                            string existingEmail = existingGuest[0][2];

                            string habitacion = existingGuest[0][3];

                            if (guest.Email != existingEmail ||
                                guest.Checkin.DayOfYear != existingCheckin.DayOfYear ||
                                guest.Checkout.DayOfYear != existingCheckout.DayOfYear ||
                                guest.RoomNumber != habitacion)
                            {
                                //int x = Global.MySqlQueries.updatedGuests(id, guest, this);
                                guest.Id = id;
                                guestsToUpdate.Add(guest, this);
                                WriteLog("EMAIL DEL UPDATE, PRODUCCION " + guest.Email);
                                updateCount++;
                            }
                            else sameCount++;
                        }
                        else
                        {
                            WriteLog("Format  data Error: email ->" + existingGuest[0][2] + "existingCheckin->" + existingGuest[0][0] + " existingCheckout->" + existingGuest[0][1]);
                        }
                    }
                }

                int insertedRows = Global.mysqlConnection.InsertBulkGuests(guestsToInsert);
                int updatedRows = Global.mysqlConnection.UpdateGuestsBulk(guestsToUpdate);

                WriteLog(insertCount + " guests INSERTED");
                WriteLog(updateCount + " guests UPDATED");
                WriteLog(sameCount + " guests NO CHANGE");

                WriteLog("Waiting next refresh...\n\n");

                return new int[3] { insertCount, updateCount, sameCount };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int[] CheckGuests(List<Guest> guests)
        {
            WriteLog("Updating Database...");
            WriteLog("Total Guests: " + guests.Count);
            try
            {
                int insertCount = 0;
                int updateCount = 0;
                int sameCount = 0;

                Dictionary<Guest, Hotel> guestsToInsert = new Dictionary<Guest, Hotel>();
                Dictionary<Guest, Hotel> guestsToUpdate = new Dictionary<Guest, Hotel>();

                foreach (Guest guest in guests)
                {
                    int id = Global.mysqlConnection.CheckGuestExists(guest);

                    if (id == 0)
                    {
                        guestsToInsert.Add(guest, this);
                        insertCount++;
                    }
                    else if (id != -1) //si no hubo error
                    {
                        List<List<string>> existingGuest = Global.mysqlConnection.GetGuestById(id);
                        DateTime existingCheckin = new DateTime();
                        DateTime existingCheckout = new DateTime();
                        if (DateTime.TryParse(existingGuest[0][0], out existingCheckin) &&
                            DateTime.TryParse(existingGuest[0][1], out existingCheckout)
                                 && existingGuest[0][0] != "" && existingGuest[0][1] != "")
                        {

                            string existingEmail = existingGuest[0][2];

                            string habitacion = existingGuest[0][3];

                            if (guest.Email != existingEmail ||
                                guest.Checkin.DayOfYear != existingCheckin.DayOfYear ||
                                guest.Checkout.DayOfYear != existingCheckout.DayOfYear ||
                                guest.RoomNumber != habitacion)
                            {
                                guest.Id = id;
                                guestsToUpdate.Add(guest, this);
                                updateCount++;
                            }
                            else
                                guestsToInsert.Add(guest, this);//prueba
                            sameCount++;
                        }
                        else
                        {
                            WriteLog("Format  data Error: email ->" + existingGuest[0][2] + "existingCheckin->" + existingGuest[0][0] + " existingCheckout->" + existingGuest[0][1]);
                        }
                    }
                }

                WriteLog("Waiting next refresh...\n\n");

                foreach (var item in guestsToInsert)
                {
                    WriteLog(item.Key.Name + " Key");
                    WriteLog(item.Value.HotelName + " Value");
                }

                return new int[] { insertCount, updateCount, sameCount };
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected int DeleteGuests(List<Guest> guests)
        {
            int deleted = 0;
            foreach (Guest guest in guests)
            {
                int id = Global.mysqlConnection.CheckGuestExists(guest);

                if (id != 0)
                {
                    int rows = Global.mysqlConnection.DeleteGuest(id);
                    deleted += rows;
                }
            }

            return deleted;
        }

        public void ChangeIntegrationStatus(Global.IntegrationType integrationType, int countRecord, int[] insertUpdateCount, string message, Global.ErrorTypes error = Global.ErrorTypes.None)
        {
            string status = "";

            if (integrationType == Global.IntegrationType.Reporting)
            {
                //Nothing...
            }
            else if (integrationType == Global.IntegrationType.ExternalConnection || integrationType == Global.IntegrationType.EmailReader)
            {
                if (countRecord > 0)
                {
                    if (insertUpdateCount[0] > 0 || insertUpdateCount[1] > 0)
                    {
                        status = Global.ErrorMessages[Global.ErrorTypes.None];
                    }
                    else
                    {
                        status = Global.ErrorMessages[Global.ErrorTypes.NoNewGuests];
                    }

                }
                else if (countRecord == 0)
                {
                    if (error == Global.ErrorTypes.None)
                    {
                        WriteLog("No new record found!\n");
                        status = Global.ErrorMessages[Global.ErrorTypes.NoNewReports];
                    }
                    else
                    {
                        status = Global.ErrorMessages[error];
                    }
                }

                //no used to webService
                LastModifiedDate = new DateTime(0);
            }
            else if (integrationType == Global.IntegrationType.Manual)
            {
                status = Global.ErrorMessages[Global.ErrorTypes.None];
            }

            WriteLog(" Integration Status: " + status + " ");
            Global.mysqlConnection.QueryInsertIntegrationLog(this.idHotel, status, filesCount, countRecord, insertUpdateCount[0], insertUpdateCount[1], LastModifiedDate, message);

        }

        protected void ChangeDDBBIntegrationStatus(string status, bool error)
        {
            Global.mysqlConnection.UpdateIntegrationStatus(this.idHotel, status, error);
        }

        protected string GetISO2FromCountry(Global.CountryCodeType countryCodeType, string info)
        {
            if (countryCodeType == Global.CountryCodeType.FullNameEs || countryCodeType == Global.CountryCodeType.FullNameEn
                || countryCodeType == Global.CountryCodeType.ISO2 || countryCodeType == Global.CountryCodeType.ISO3)
            {
                if (Global.countries.Iso2ByFullNameEs.ContainsKey(Global.quitAccents(info).ToUpper()))
                {
                    //fullnameEs
                    return Global.countries.Iso2ByFullNameEs[Global.quitAccents(info).ToUpper()];
                }
                else if (Global.countries.Iso2ByFullNameEn.ContainsKey(Global.quitAccents(info).ToUpper()))
                {
                    //fullnameEn
                    return Global.countries.Iso2ByFullNameEn[Global.quitAccents(info).ToUpper()];
                }
                else if (Global.countries.Iso2ByISO2.ContainsKey(info.ToUpper()))
                {
                    //iso2
                    return Global.countries.Iso2ByISO2[info.ToUpper()];
                }
                else if (Global.countries.Iso2ByISO3.ContainsKey(info.ToUpper()))
                {
                    //iso3
                    return Global.countries.Iso2ByISO3[info.ToUpper()];
                }
                else if (Global.countries.Iso2BySpecialCases.ContainsKey(info.ToUpper()))
                {
                    //specials cases
                    return Global.countries.Iso2BySpecialCases[info.ToUpper()];
                }
            }
            return Global.ErrorMessages[Global.ErrorTypes.Nofound];
        }

        protected int GetLanguageFromCountry(string iso2)
        {
            if (Global.countries.LanguagesByISO2.ContainsKey(iso2.ToUpper()))
                return Int32.Parse(Global.countries.LanguagesByISO2[iso2.ToUpper()]);
            else
                //Defaul 1:Spanish
                return 1;
        }

        public void CleanGuestsFormats(List<Guest> guests)
        {
            foreach (Guest guest in guests)
            {
                guest.Channel = guest.Channel.Replace("'", "");
                guest.Email = guest.Email.Replace("'", "");
                guest.Gender = guest.Gender.Replace("'", "");
                guest.Name = guest.Name.Replace("'", "");
                guest.LastName = guest.LastName.Replace("'", "");
                guest.PhoneNumber = guest.PhoneNumber.Replace("'", "");
                guest.RoomNumber = guest.RoomNumber.Replace("'", "");
            }
        }

        private Boolean ValidateEmail(String email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool CheckIfIntegration()
        {
            if (this.flagCliente == 1 &&
                (this.products.Fidelity || this.products.FidelityOnSite || this.products.Iconcierge ||
                this.products.IconciergePrellegada || this.products.ReviewExpress))
            {
                return true;
            }
            else return false;
        }



    }
}