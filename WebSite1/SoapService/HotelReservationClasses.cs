﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace SoapService
{
    [XmlRoot(ElementName = "RequestorID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RequestorID
    {
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
    }

    [XmlRoot(ElementName = "Source", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Source
    {
        [XmlElement(ElementName = "RequestorID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RequestorID RequestorID { get; set; }
        [XmlElement(ElementName = "BookingChannel", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public BookingChannel BookingChannel { get; set; }
    }
    [XmlRoot(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Creds : SoapHeader
    {
        public string Username;
        public string Password;
    }

    [XmlRoot(ElementName = "CompanyName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class CompanyName
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "BookingChannel", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class BookingChannel
    {
        [XmlElement(ElementName = "CompanyName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CompanyName CompanyName { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "Primary")]
        public string Primary { get; set; }
    }

    [XmlRoot(ElementName = "POS", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class POS
    {
        [XmlElement(ElementName = "Source", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<Source> Source { get; set; }
    }

    [XmlRoot(ElementName = "UniqueID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class UniqueID
    {
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "ID_Context")]
        public string ID_Context { get; set; }
    }

    [XmlRoot(ElementName = "RoomDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomDescription
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "DetailDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class DetailDescription
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "AdditionalDetail", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AdditionalDetail
    {
        [XmlElement(ElementName = "DetailDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public DetailDescription DetailDescription { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "AdditionalDetails", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AdditionalDetails
    {
        [XmlElement(ElementName = "AdditionalDetail", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AdditionalDetail AdditionalDetail { get; set; }
    }

    [XmlRoot(ElementName = "RoomType", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomType
    {
        [XmlElement(ElementName = "RoomDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomDescription RoomDescription { get; set; }
        [XmlElement(ElementName = "AdditionalDetails", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AdditionalDetails AdditionalDetails { get; set; }
        [XmlAttribute(AttributeName = "RoomType")]
        public string _RoomType { get; set; }
        [XmlAttribute(AttributeName = "RoomTypeCode")]
        public string RoomTypeCode { get; set; }
        [XmlAttribute(AttributeName = "RoomCategory")]
        public string RoomCategory { get; set; }
        [XmlAttribute(AttributeName = "RoomID")]
        public string RoomID { get; set; }
        [XmlAttribute(AttributeName = "NonSmoking")]
        public string NonSmoking { get; set; }
        [XmlAttribute(AttributeName = "Configuration")]
        public string Configuration { get; set; }
    }

    [XmlRoot(ElementName = "RoomTypes", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomTypes
    {
        [XmlElement(ElementName = "RoomType", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomType RoomType { get; set; }
    }

    [XmlRoot(ElementName = "RatePlanDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RatePlanDescription
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "RatePlanInclusionDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RatePlanInclusionDescription
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "RatePlanInclusions", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RatePlanInclusions
    {
        [XmlElement(ElementName = "RatePlanInclusionDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RatePlanInclusionDescription RatePlanInclusionDescription { get; set; }
        [XmlAttribute(AttributeName = "TaxInclusive")]
        public string TaxInclusive { get; set; }
        [XmlAttribute(AttributeName = "ServiceFeeInclusive")]
        public string ServiceFeeInclusive { get; set; }
    }

    [XmlRoot(ElementName = "MealsIncluded", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class MealsIncluded
    {
        [XmlAttribute(AttributeName = "MealPlanIndicator")]
        public string MealPlanIndicator { get; set; }
        [XmlAttribute(AttributeName = "MealPlanCodes")]
        public string MealPlanCodes { get; set; }
    }

    [XmlRoot(ElementName = "RatePlan", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RatePlan
    {
        [XmlElement(ElementName = "RatePlanDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RatePlanDescription RatePlanDescription { get; set; }
        [XmlElement(ElementName = "RatePlanInclusions", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RatePlanInclusions RatePlanInclusions { get; set; }
        [XmlElement(ElementName = "MealsIncluded", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public MealsIncluded MealsIncluded { get; set; }
        [XmlElement(ElementName = "AdditionalDetails", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AdditionalDetails AdditionalDetails { get; set; }
        [XmlAttribute(AttributeName = "RatePlanCode")]
        public string RatePlanCode { get; set; }
        [XmlAttribute(AttributeName = "EffectiveDate")]
        public string EffectiveDate { get; set; }
        [XmlAttribute(AttributeName = "ExpireDate")]
        public string ExpireDate { get; set; }
        [XmlAttribute(AttributeName = "RatePlanName")]
        public string RatePlanName { get; set; }
    }

    [XmlRoot(ElementName = "RatePlans", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RatePlans
    {
        [XmlElement(ElementName = "RatePlan", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RatePlan RatePlan { get; set; }
    }

    [XmlRoot(ElementName = "TaxDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class TaxDescription
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Tax", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Tax
    {
        [XmlElement(ElementName = "TaxDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TaxDescription TaxDescription { get; set; }
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "Amount")]
        public string Amount { get; set; }
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
        [XmlAttribute(AttributeName = "Percent")]
        public string Percent { get; set; }
    }

    [XmlRoot(ElementName = "Taxes", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Taxes
    {
        [XmlElement(ElementName = "Tax", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<Tax> Tax { get; set; }
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
        [XmlAttribute(AttributeName = "Amount")]
        public string Amount { get; set; }
    }

    [XmlRoot(ElementName = "Base", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Base
    {
        [XmlElement(ElementName = "Taxes", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Taxes Taxes { get; set; }
        [XmlAttribute(AttributeName = "AmountBeforeTax")]
        public string AmountBeforeTax { get; set; }
        [XmlAttribute(AttributeName = "AmountAfterTax")]
        public string AmountAfterTax { get; set; }
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
    }

    [XmlRoot(ElementName = "Total", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Total
    {
        [XmlElement(ElementName = "Taxes", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Taxes Taxes { get; set; }
        [XmlAttribute(AttributeName = "AmountBeforeTax")]
        public string AmountBeforeTax { get; set; }
        [XmlAttribute(AttributeName = "AmountAfterTax")]
        public string AmountAfterTax { get; set; }
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
    }

    [XmlRoot(ElementName = "Rate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Rate
    {
        [XmlElement(ElementName = "Base", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Base Base { get; set; }
        [XmlElement(ElementName = "Total", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Total Total { get; set; }
        [XmlAttribute(AttributeName = "EffectiveDate")]
        public string EffectiveDate { get; set; }
        [XmlAttribute(AttributeName = "ExpireDate")]
        public string ExpireDate { get; set; }
        [XmlAttribute(AttributeName = "UnitMultiplier")]
        public string UnitMultiplier { get; set; }
    }

    [XmlRoot(ElementName = "Rates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Rates
    {
        [XmlElement(ElementName = "Rate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<Rate> Rate { get; set; }
    }

    [XmlRoot(ElementName = "ServiceRPH", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ServiceRPH
    {
        [XmlAttribute(AttributeName = "RPH")]
        public string RPH { get; set; }
    }

    [XmlRoot(ElementName = "ServiceRPHs", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ServiceRPHs
    {
        [XmlElement(ElementName = "ServiceRPH", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ServiceRPH ServiceRPH { get; set; }
    }

    [XmlRoot(ElementName = "RoomRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomRate
    {
        [XmlElement(ElementName = "Rates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Rates Rates { get; set; }
        [XmlElement(ElementName = "ServiceRPHs", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ServiceRPHs ServiceRPHs { get; set; }
        [XmlAttribute(AttributeName = "InvBlockCode")]
        public string InvBlockCode { get; set; }
        [XmlAttribute(AttributeName = "NumberOfUnits")]
        public string NumberOfUnits { get; set; }
        [XmlAttribute(AttributeName = "RoomID")]
        public string RoomID { get; set; }
        [XmlAttribute(AttributeName = "RoomTypeCode")]
        public string RoomTypeCode { get; set; }
        [XmlAttribute(AttributeName = "RatePlanCode")]
        public string RatePlanCode { get; set; }
        [XmlAttribute(AttributeName = "RatePlanCategory")]
        public string RatePlanCategory { get; set; }
        [XmlAttribute(AttributeName = "EffectiveDate")]
        public string EffectiveDate { get; set; }
        [XmlAttribute(AttributeName = "ExpireDate")]
        public string ExpireDate { get; set; }
    }

    [XmlRoot(ElementName = "RoomRates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomRates
    {
        [XmlElement(ElementName = "RoomRate", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomRate RoomRate { get; set; }
    }

    [XmlRoot(ElementName = "GuestCount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuestCount
    {
        [XmlAttribute(AttributeName = "AgeQualifyingCode")]
        public string AgeQualifyingCode { get; set; }
        [XmlAttribute(AttributeName = "Age")]
        public string Age { get; set; }
        [XmlAttribute(AttributeName = "Count")]
        public string Count { get; set; }
        [XmlAttribute(AttributeName = "AgeBucket")]
        public string AgeBucket { get; set; }
    }

    [XmlRoot(ElementName = "GuestCounts", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuestCounts
    {
        [XmlElement(ElementName = "GuestCount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<GuestCount> GuestCount { get; set; }
    }

    [XmlRoot(ElementName = "TimeSpan", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class TimeSpan
    {
        [XmlAttribute(AttributeName = "Start")]
        public string Start { get; set; }
        [XmlAttribute(AttributeName = "End")]
        public string End { get; set; }
    }

    [XmlRoot(ElementName = "CardNumber", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class CardNumber
    {
        [XmlAttribute(AttributeName = "Mask")]
        public string Mask { get; set; }
        [XmlAttribute(AttributeName = "Token")]
        public string Token { get; set; }
        [XmlAttribute(AttributeName = "TokenProviderID")]
        public string TokenProviderID { get; set; }
    }

    [XmlRoot(ElementName = "PaymentCard", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class PaymentCard
    {
        [XmlElement(ElementName = "CardHolderName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string CardHolderName { get; set; }
        [XmlElement(ElementName = "CardNumber", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CardNumber CardNumber { get; set; }
        [XmlAttribute(AttributeName = "CardCode")]
        public string CardCode { get; set; }
        [XmlAttribute(AttributeName = "EffectiveDate")]
        public string EffectiveDate { get; set; }
        [XmlAttribute(AttributeName = "ExpireDate")]
        public string ExpireDate { get; set; }
    }

    [XmlRoot(ElementName = "GuaranteeAccepted", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuaranteeAccepted
    {
        [XmlElement(ElementName = "PaymentCard", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public PaymentCard PaymentCard { get; set; }
        [XmlAttribute(AttributeName = "PaymentTransactionTypeCode")]
        public string PaymentTransactionTypeCode { get; set; }
        [XmlElement(ElementName = "Voucher", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Voucher Voucher { get; set; }
        [XmlElement(ElementName = "DirectBill", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public DirectBill DirectBill { get; set; }
    }

    [XmlRoot(ElementName = "Voucher", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Voucher
    {
        [XmlAttribute(AttributeName = "SeriesCode")]
        public string SeriesCode { get; set; }
    }

    [XmlRoot(ElementName = "DirectBill", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class DirectBill
    {
        [XmlAttribute(AttributeName = "DirectBill_ID")]
        public string DirectBill_ID { get; set; }
    }

    [XmlRoot(ElementName = "GuaranteesAccepted", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuaranteesAccepted
    {
        [XmlElement(ElementName = "GuaranteeAccepted", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<GuaranteeAccepted> GuaranteeAccepted { get; set; }
    }

    [XmlRoot(ElementName = "GuaranteeDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuaranteeDescription
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Guarantee", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Guarantee
    {
        [XmlElement(ElementName = "GuaranteesAccepted", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public GuaranteesAccepted GuaranteesAccepted { get; set; }
        [XmlElement(ElementName = "GuaranteeDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public GuaranteeDescription GuaranteeDescription { get; set; }
        [XmlAttribute(AttributeName = "GuaranteeCode")]
        public string GuaranteeCode { get; set; }
        [XmlAttribute(AttributeName = "GuaranteeType")]
        public string GuaranteeType { get; set; }
    }

    [XmlRoot(ElementName = "AcceptedPayment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AcceptedPayment
    {
        [XmlElement(ElementName = "PaymentCard", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public PaymentCard PaymentCard { get; set; }
        [XmlAttribute(AttributeName = "PaymentTransactionTypeCode")]
        public string PaymentTransactionTypeCode { get; set; }
        [XmlElement(ElementName = "Voucher", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Voucher Voucher { get; set; }
        [XmlElement(ElementName = "DirectBill", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public DirectBill DirectBill { get; set; }
    }

    [XmlRoot(ElementName = "AcceptedPayments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AcceptedPayments
    {
        [XmlElement(ElementName = "AcceptedPayment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AcceptedPayment AcceptedPayment { get; set; }
    }

    [XmlRoot(ElementName = "AmountPercent", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AmountPercent
    {
        [XmlElement(ElementName = "Taxes", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Taxes Taxes { get; set; }
        [XmlAttribute(AttributeName = "Percent")]
        public string Percent { get; set; }
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
        [XmlAttribute(AttributeName = "Amount")]
        public string Amount { get; set; }
        [XmlAttribute(AttributeName = "NmbrOfNights")]
        public string NmbrOfNights { get; set; }
    }

    [XmlRoot(ElementName = "Deadline", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Deadline
    {
        [XmlAttribute(AttributeName = "AbsoluteDeadline")]
        public string AbsoluteDeadline { get; set; }
        [XmlAttribute(AttributeName = "OffsetTimeUnit")]
        public string OffsetTimeUnit { get; set; }
        [XmlAttribute(AttributeName = "OffsetUnitMultiplier")]
        public string OffsetUnitMultiplier { get; set; }
        [XmlAttribute(AttributeName = "OffsetDropTime")]
        public string OffsetDropTime { get; set; }
    }

    [XmlRoot(ElementName = "Description", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Description
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "StateProv", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class StateProv
    {
        [XmlAttribute(AttributeName = "StateCode")]
        public string StateCode { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "CountryName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class CountryName
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Address", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Address
    {
        [XmlElement(ElementName = "AddressLine", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string AddressLine { get; set; }
        [XmlElement(ElementName = "CityName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string CityName { get; set; }
        [XmlElement(ElementName = "PostalCode", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "StateProv", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public StateProv StateProv { get; set; }
        [XmlElement(ElementName = "CountryName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CountryName CountryName { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "CompanyName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CompanyName CompanyName { get; set; }
        [XmlElement(ElementName = "AddresseeName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AddresseeName AddresseeName { get; set; }
    }

    [XmlRoot(ElementName = "GuaranteePayment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class GuaranteePayment
    {
        [XmlElement(ElementName = "AcceptedPayments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AcceptedPayments AcceptedPayments { get; set; }
        [XmlElement(ElementName = "AmountPercent", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AmountPercent AmountPercent { get; set; }
        [XmlElement(ElementName = "Deadline", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Deadline Deadline { get; set; }
        [XmlElement(ElementName = "Description", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Description Description { get; set; }
        [XmlElement(ElementName = "Address", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Address Address { get; set; }
    }

    [XmlRoot(ElementName = "DepositPayments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class DepositPayments
    {
        [XmlElement(ElementName = "GuaranteePayment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<GuaranteePayment> GuaranteePayment { get; set; }
    }

    [XmlRoot(ElementName = "DiscountReason", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class DiscountReason
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Discount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Discount
    {
        [XmlElement(ElementName = "Taxes", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Taxes Taxes { get; set; }
        [XmlElement(ElementName = "DiscountReason", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public DiscountReason DiscountReason { get; set; }
        [XmlAttribute(AttributeName = "TaxInclusive")]
        public string TaxInclusive { get; set; }
        [XmlAttribute(AttributeName = "Percent")]
        public string Percent { get; set; }
        [XmlAttribute(AttributeName = "DiscountCode")]
        public string DiscountCode { get; set; }
        [XmlAttribute(AttributeName = "AmountBeforeTax")]
        public string AmountBeforeTax { get; set; }
        [XmlAttribute(AttributeName = "AmountAfterTax")]
        public string AmountAfterTax { get; set; }
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
    }

    [XmlRoot(ElementName = "ResGuestRPH", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ResGuestRPH
    {
        [XmlAttribute(AttributeName = "RPH")]
        public string RPH { get; set; }
    }

    [XmlRoot(ElementName = "ResGuestRPHs", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ResGuestRPHs
    {
        [XmlElement(ElementName = "ResGuestRPH", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<ResGuestRPH> ResGuestRPH { get; set; }
    }

    [XmlRoot(ElementName = "Membership", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Membership
    {
        [XmlAttribute(AttributeName = "ProgramCode")]
        public string ProgramCode { get; set; }
        [XmlAttribute(AttributeName = "AccountID")]
        public string AccountID { get; set; }
    }

    [XmlRoot(ElementName = "Memberships", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Memberships
    {
        [XmlElement(ElementName = "Membership", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<Membership> Membership { get; set; }
    }

    [XmlRoot(ElementName = "Comment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Comment
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "GuestViewable")]
        public string GuestViewable { get; set; }
    }

    [XmlRoot(ElementName = "Comments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Comments
    {
        [XmlElement(ElementName = "Comment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Comment Comment { get; set; }
    }

    [XmlRoot(ElementName = "SpecialRequest", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class SpecialRequest
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "RequestCode")]
        public string RequestCode { get; set; }
        [XmlAttribute(AttributeName = "CodeContext")]
        public string CodeContext { get; set; }
    }

    [XmlRoot(ElementName = "SpecialRequests", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class SpecialRequests
    {
        [XmlElement(ElementName = "SpecialRequest", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<SpecialRequest> SpecialRequest { get; set; }
    }

    [XmlRoot(ElementName = "RoomStay", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomStay
    {
        [XmlElement(ElementName = "RoomTypes", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomTypes RoomTypes { get; set; }
        [XmlElement(ElementName = "RatePlans", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RatePlans RatePlans { get; set; }
        [XmlElement(ElementName = "RoomRates", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomRates RoomRates { get; set; }
        [XmlElement(ElementName = "GuestCounts", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public GuestCounts GuestCounts { get; set; }
        [XmlElement(ElementName = "TimeSpan", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TimeSpan TimeSpan { get; set; }
        [XmlElement(ElementName = "Guarantee", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Guarantee Guarantee { get; set; }
        [XmlElement(ElementName = "DepositPayments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public DepositPayments DepositPayments { get; set; }
        [XmlElement(ElementName = "Discount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Discount Discount { get; set; }
        [XmlElement(ElementName = "Total", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Total Total { get; set; }
        [XmlElement(ElementName = "ResGuestRPHs", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ResGuestRPHs ResGuestRPHs { get; set; }
        [XmlElement(ElementName = "Memberships", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Memberships Memberships { get; set; }
        [XmlElement(ElementName = "Comments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Comments Comments { get; set; }
        [XmlElement(ElementName = "SpecialRequests", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public SpecialRequests SpecialRequests { get; set; }
        [XmlAttribute(AttributeName = "MarketCode")]
        public string MarketCode { get; set; }
        [XmlAttribute(AttributeName = "SourceOfBusiness")]
        public string SourceOfBusiness { get; set; }
        [XmlAttribute(AttributeName = "PromotionCode")]
        public string PromotionCode { get; set; }
    }

    [XmlRoot(ElementName = "RoomStays", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RoomStays
    {
        [XmlElement(ElementName = "RoomStay", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomStay RoomStay { get; set; }
    }

    [XmlRoot(ElementName = "Price", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Price
    {
        [XmlElement(ElementName = "Total", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Total Total { get; set; }
    }

    [XmlRoot(ElementName = "ServiceDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ServiceDescription
    {
        [XmlElement(ElementName = "Text", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ServiceDetails", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ServiceDetails
    {
        [XmlElement(ElementName = "GuestCounts", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public GuestCounts GuestCounts { get; set; }
        [XmlElement(ElementName = "TimeSpan", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TimeSpan TimeSpan { get; set; }
        [XmlElement(ElementName = "Comments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Comments Comments { get; set; }
        [XmlElement(ElementName = "ServiceDescription", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ServiceDescription ServiceDescription { get; set; }
    }

    [XmlRoot(ElementName = "Service", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Service
    {
        [XmlElement(ElementName = "Price", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Price Price { get; set; }
        [XmlElement(ElementName = "ServiceDetails", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ServiceDetails ServiceDetails { get; set; }
        [XmlAttribute(AttributeName = "ServicePricingType")]
        public string ServicePricingType { get; set; }
        [XmlAttribute(AttributeName = "ServiceCategoryCode")]
        public string ServiceCategoryCode { get; set; }
        [XmlAttribute(AttributeName = "ServiceInventoryCode")]
        public string ServiceInventoryCode { get; set; }
        [XmlAttribute(AttributeName = "Inclusive")]
        public string Inclusive { get; set; }
        [XmlAttribute(AttributeName = "Quantity")]
        public string Quantity { get; set; }
        [XmlAttribute(AttributeName = "ServiceRPH")]
        public string ServiceRPH { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlAttribute(AttributeName = "ID_Context")]
        public string ID_Context { get; set; }
    }

    [XmlRoot(ElementName = "ServiceCategory", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ServiceCategory
    {
        [XmlAttribute(AttributeName = "ServiceCategoryCode")]
        public string ServiceCategoryCode { get; set; }
    }

    [XmlRoot(ElementName = "Services", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Services
    {
        [XmlElement(ElementName = "Service", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<Service> Service { get; set; }
        [XmlElement(ElementName = "ServiceCategory", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<ServiceCategory> ServiceCategory { get; set; }
    }

    [XmlRoot(ElementName = "BillingInstructionCode", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class BillingInstructionCode
    {
        [XmlElement(ElementName = "ResGuestRPH", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ResGuestRPH ResGuestRPH { get; set; }
        [XmlAttribute(AttributeName = "BillingCode")]
        public string BillingCode { get; set; }
        [XmlAttribute(AttributeName = "AccountNumber")]
        public string AccountNumber { get; set; }
        [XmlAttribute(AttributeName = "Start")]
        public string Start { get; set; }
        [XmlAttribute(AttributeName = "End")]
        public string End { get; set; }
        [XmlAttribute(AttributeName = "AuthorizationCode")]
        public string AuthorizationCode { get; set; }
        [XmlAttribute(AttributeName = "Description")]
        public string Description { get; set; }
    }

    [XmlRoot(ElementName = "PersonName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class PersonName
    {
        [XmlElement(ElementName = "NamePrefix", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string NamePrefix { get; set; }
        [XmlElement(ElementName = "GivenName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string GivenName { get; set; }
        [XmlElement(ElementName = "MiddleName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string MiddleName { get; set; }
        [XmlElement(ElementName = "Surname", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Surname { get; set; }
        [XmlElement(ElementName = "NameSuffix", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string NameSuffix { get; set; }
        [XmlElement(ElementName = "NameTitle", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string NameTitle { get; set; }
        [XmlAttribute(AttributeName = "NameType")]
        public string NameType { get; set; }
        [XmlAttribute(AttributeName = "Language")]
        public string Language { get; set; }
    }

    [XmlRoot(ElementName = "Telephone", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Telephone
    {
        [XmlAttribute(AttributeName = "PhoneLocationType")]
        public string PhoneLocationType { get; set; }
        [XmlAttribute(AttributeName = "PhoneTechType")]
        public string PhoneTechType { get; set; }
        [XmlAttribute(AttributeName = "CountryAccessCode")]
        public string CountryAccessCode { get; set; }
        [XmlAttribute(AttributeName = "AreaCityCode")]
        public string AreaCityCode { get; set; }
        [XmlAttribute(AttributeName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
        [XmlAttribute(AttributeName = "Remark")]
        public string Remark { get; set; }
        [XmlAttribute(AttributeName = "FormattedInd")]
        public string FormattedInd { get; set; }
        [XmlAttribute(AttributeName = "DefaultInd")]
        public string DefaultInd { get; set; }
    }

    [XmlRoot(ElementName = "Email", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Email
    {
        [XmlAttribute(AttributeName = "DefaultInd")]
        public string DefaultInd { get; set; }
        [XmlAttribute(AttributeName = "EmailType")]
        public string EmailType { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "AddresseeName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AddresseeName
    {
        [XmlElement(ElementName = "NamePrefix", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string NamePrefix { get; set; }
        [XmlElement(ElementName = "GivenName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string GivenName { get; set; }
        [XmlElement(ElementName = "MiddleName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string MiddleName { get; set; }
        [XmlElement(ElementName = "Surname", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string Surname { get; set; }
        [XmlElement(ElementName = "NameSuffix", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string NameSuffix { get; set; }
        [XmlElement(ElementName = "NameTitle", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string NameTitle { get; set; }
        [XmlAttribute(AttributeName = "NameType")]
        public string NameType { get; set; }
        [XmlAttribute(AttributeName = "Language")]
        public string Language { get; set; }
    }

    [XmlRoot(ElementName = "RelatedTraveler", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class RelatedTraveler
    {
        [XmlElement(ElementName = "UniqueID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public UniqueID UniqueID { get; set; }
        [XmlElement(ElementName = "PersonName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public PersonName PersonName { get; set; }
        [XmlAttribute(AttributeName = "Relation")]
        public string Relation { get; set; }
        [XmlAttribute(AttributeName = "BirthDate")]
        public string BirthDate { get; set; }
    }

    [XmlRoot(ElementName = "CustLoyalty", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class CustLoyalty
    {
        [XmlAttribute(AttributeName = "ProgramID")]
        public string ProgramID { get; set; }
        [XmlAttribute(AttributeName = "MembershipID")]
        public string MembershipID { get; set; }
        [XmlAttribute(AttributeName = "LoyalLevel")]
        public string LoyalLevel { get; set; }
        [XmlAttribute(AttributeName = "LoyalLevelCode")]
        public string LoyalLevelCode { get; set; }
        [XmlAttribute(AttributeName = "SignupDate")]
        public string SignupDate { get; set; }
        [XmlAttribute(AttributeName = "EffectiveDate")]
        public string EffectiveDate { get; set; }
        [XmlAttribute(AttributeName = "ExpireDate")]
        public string ExpireDate { get; set; }
        [XmlAttribute(AttributeName = "Remark")]
        public string Remark { get; set; }
    }

    [XmlRoot(ElementName = "Customer", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Customer
    {
        [XmlElement(ElementName = "PersonName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public PersonName PersonName { get; set; }
        [XmlElement(ElementName = "Telephone", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Telephone Telephone { get; set; }
        [XmlElement(ElementName = "Email", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Email Email { get; set; }
        [XmlElement(ElementName = "Address", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "RelatedTraveler", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RelatedTraveler RelatedTraveler { get; set; }
        [XmlElement(ElementName = "CustLoyalty", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CustLoyalty CustLoyalty { get; set; }
        [XmlAttribute(AttributeName = "VIP_Indicator")]
        public string VIP_Indicator { get; set; }
        [XmlAttribute(AttributeName = "CustomerValue")]
        public string CustomerValue { get; set; }
        [XmlAttribute(AttributeName = "BirthDate")]
        public string BirthDate { get; set; }
    }

    [XmlRoot(ElementName = "AddressInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class AddressInfo
    {
        [XmlElement(ElementName = "AddressLine", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string AddressLine { get; set; }
        [XmlElement(ElementName = "CityName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string CityName { get; set; }
        [XmlElement(ElementName = "PostalCode", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "StateProv", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public StateProv StateProv { get; set; }
        [XmlElement(ElementName = "CountryName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CountryName CountryName { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "TelephoneInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class TelephoneInfo
    {
        [XmlAttribute(AttributeName = "PhoneLocationType")]
        public string PhoneLocationType { get; set; }
        [XmlAttribute(AttributeName = "PhoneTechType")]
        public string PhoneTechType { get; set; }
        [XmlAttribute(AttributeName = "CountryAccessCode")]
        public string CountryAccessCode { get; set; }
        [XmlAttribute(AttributeName = "AreaCityCode")]
        public string AreaCityCode { get; set; }
        [XmlAttribute(AttributeName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
        [XmlAttribute(AttributeName = "Remark")]
        public string Remark { get; set; }
        [XmlAttribute(AttributeName = "FormattedInd")]
        public string FormattedInd { get; set; }
        [XmlAttribute(AttributeName = "DefaultInd")]
        public string DefaultInd { get; set; }
    }

    [XmlRoot(ElementName = "ContactPerson", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ContactPerson
    {
        [XmlElement(ElementName = "PersonName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public PersonName PersonName { get; set; }
        [XmlElement(ElementName = "Telephone", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Telephone Telephone { get; set; }
        [XmlElement(ElementName = "Address", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "Email", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Email Email { get; set; }
    }

    [XmlRoot(ElementName = "CompanyInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class CompanyInfo
    {
        [XmlElement(ElementName = "CompanyName", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CompanyName CompanyName { get; set; }
        [XmlElement(ElementName = "AddressInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public AddressInfo AddressInfo { get; set; }
        [XmlElement(ElementName = "TelephoneInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TelephoneInfo TelephoneInfo { get; set; }
        [XmlElement(ElementName = "Email", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Email Email { get; set; }
        [XmlElement(ElementName = "ContactPerson", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ContactPerson ContactPerson { get; set; }
    }

    [XmlRoot(ElementName = "Profile", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Profile
    {
        [XmlElement(ElementName = "Customer", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Customer Customer { get; set; }
        [XmlElement(ElementName = "CompanyInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CompanyInfo CompanyInfo { get; set; }
        [XmlAttribute(AttributeName = "ProfileType")]
        public string ProfileType { get; set; }
        [XmlAttribute(AttributeName = "ShareAllOptOutInd")]
        public string ShareAllOptOutInd { get; set; }
        [XmlAttribute(AttributeName = "ShareAllMarketInd")]
        public string ShareAllMarketInd { get; set; }
    }

    [XmlRoot(ElementName = "ProfileInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ProfileInfo
    {
        [XmlElement(ElementName = "UniqueID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public UniqueID UniqueID { get; set; }
        [XmlElement(ElementName = "Profile", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Profile Profile { get; set; }
    }

    [XmlRoot(ElementName = "Profiles", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Profiles
    {
        [XmlElement(ElementName = "ProfileInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<ProfileInfo> ProfileInfo { get; set; }
    }

    [XmlRoot(ElementName = "TransportInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class TransportInfo
    {
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
        [XmlAttribute(AttributeName = "Time")]
        public string Time { get; set; }
    }

    [XmlRoot(ElementName = "ArrivalTransport", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ArrivalTransport
    {
        [XmlElement(ElementName = "TransportInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TransportInfo TransportInfo { get; set; }
    }

    [XmlRoot(ElementName = "DepartureTransport", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class DepartureTransport
    {
        [XmlElement(ElementName = "TransportInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TransportInfo TransportInfo { get; set; }
    }

    [XmlRoot(ElementName = "ResGuest", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ResGuest
    {
        [XmlElement(ElementName = "Profiles", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Profiles Profiles { get; set; }
        [XmlElement(ElementName = "SpecialRequests", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public SpecialRequests SpecialRequests { get; set; }
        [XmlElement(ElementName = "Comments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Comments Comments { get; set; }
        [XmlElement(ElementName = "ServiceRPHs", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ServiceRPHs ServiceRPHs { get; set; }
        [XmlElement(ElementName = "ArrivalTransport", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ArrivalTransport ArrivalTransport { get; set; }
        [XmlElement(ElementName = "DepartureTransport", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public DepartureTransport DepartureTransport { get; set; }
        [XmlAttribute(AttributeName = "ResGuestRPH")]
        public string ResGuestRPH { get; set; }
        [XmlAttribute(AttributeName = "AgeQualifyingCode")]
        public string AgeQualifyingCode { get; set; }
        [XmlAttribute(AttributeName = "ArrivalTime")]
        public string ArrivalTime { get; set; }
        [XmlAttribute(AttributeName = "PrimaryIndicator")]
        public string PrimaryIndicator { get; set; }
        [XmlAttribute(AttributeName = "Age")]
        public string Age { get; set; }
    }

    [XmlRoot(ElementName = "ResGuests", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ResGuests
    {
        [XmlElement(ElementName = "ResGuest", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public List<ResGuest> ResGuest { get; set; }
    }

    [XmlRoot(ElementName = "HotelReservationID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class HotelReservationID
    {
        [XmlAttribute(AttributeName = "ResID_Type")]
        public string ResID_Type { get; set; }
        [XmlAttribute(AttributeName = "ResID_Value")]
        public string ResID_Value { get; set; }
        [XmlAttribute(AttributeName = "ResID_Source")]
        public string ResID_Source { get; set; }
    }

    [XmlRoot(ElementName = "HotelReservationIDs", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class HotelReservationIDs
    {
        [XmlElement(ElementName = "HotelReservationID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public HotelReservationID HotelReservationID { get; set; }
    }

    [XmlRoot(ElementName = "CommissionPayableAmount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class CommissionPayableAmount
    {
        [XmlAttribute(AttributeName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
        [XmlAttribute(AttributeName = "Amount")]
        public string Amount { get; set; }
    }

    [XmlRoot(ElementName = "TotalCommissions", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class TotalCommissions
    {
        [XmlElement(ElementName = "UniqueID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public UniqueID UniqueID { get; set; }
        [XmlElement(ElementName = "CommissionPayableAmount", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public CommissionPayableAmount CommissionPayableAmount { get; set; }
        [XmlElement(ElementName = "Comment", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Comment Comment { get; set; }
    }

    [XmlRoot(ElementName = "BasicPropertyInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class BasicPropertyInfo
    {
        [XmlAttribute(AttributeName = "ChainCode")]
        public string ChainCode { get; set; }
        [XmlAttribute(AttributeName = "BrandCode")]
        public string BrandCode { get; set; }
        [XmlAttribute(AttributeName = "HotelCode")]
        public string HotelCode { get; set; }
        [XmlAttribute(AttributeName = "HotelName")]
        public string HotelName { get; set; }
    }

    [XmlRoot(ElementName = "ResGlobalInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ResGlobalInfo
    {
        [XmlElement(ElementName = "GuestCounts", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public GuestCounts GuestCounts { get; set; }
        [XmlElement(ElementName = "TimeSpan", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TimeSpan TimeSpan { get; set; }
        [XmlElement(ElementName = "Memberships", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Memberships Memberships { get; set; }
        [XmlElement(ElementName = "Comments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Comments Comments { get; set; }
        [XmlElement(ElementName = "SpecialRequests", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public SpecialRequests SpecialRequests { get; set; }
        [XmlElement(ElementName = "Guarantee", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Guarantee Guarantee { get; set; }
        [XmlElement(ElementName = "DepositPayments", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public DepositPayments DepositPayments { get; set; }
        [XmlElement(ElementName = "Total", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Total Total { get; set; }
        [XmlElement(ElementName = "HotelReservationIDs", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public HotelReservationIDs HotelReservationIDs { get; set; }
        [XmlElement(ElementName = "Profiles", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Profiles Profiles { get; set; }
        [XmlElement(ElementName = "TotalCommissions", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public TotalCommissions TotalCommissions { get; set; }
        [XmlElement(ElementName = "BasicPropertyInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public BasicPropertyInfo BasicPropertyInfo { get; set; }
    }

    [XmlRoot(ElementName = "HotelReservation", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class Reservation
    {
        [XmlElement(ElementName = "POS", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public POS POS { get; set; }
        [XmlElement(ElementName = "UniqueID", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public UniqueID UniqueID { get; set; }
        [XmlElement(ElementName = "RoomStays", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public RoomStays RoomStays { get; set; }
        [XmlElement(ElementName = "Services", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Services Services { get; set; }
        [XmlElement(ElementName = "BillingInstructionCode", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public BillingInstructionCode BillingInstructionCode { get; set; }
        [XmlElement(ElementName = "ResGuests", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ResGuests ResGuests { get; set; }
        [XmlElement(ElementName = "ResGlobalInfo", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public ResGlobalInfo ResGlobalInfo { get; set; }
        [XmlAttribute(AttributeName = "ResStatus")]
        public string ResStatus { get; set; }
        [XmlAttribute(AttributeName = "CreateDateTime")]
        public string CreateDateTime { get; set; }
        [XmlAttribute(AttributeName = "CreatorID")]
        public string CreatorID { get; set; }
        [XmlAttribute(AttributeName = "LastModifyDateTime")]
        public string LastModifyDateTime { get; set; }
        [XmlAttribute(AttributeName = "LastModifierID")]
        public string LastModifierID { get; set; }
    }

    [XmlRoot(ElementName = "HotelReservations", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class HotelReservations
    {
        [XmlElement(ElementName = "HotelReservation", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public Reservation HotelReservation { get; set; }
    }

    [XmlRoot(ElementName = "OTA_HotelResNotifRequest", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class OTA_HotelResNotifRequest
    {
        [XmlElement(ElementName = "HotelReservations", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public HotelReservations HotelReservations { get; set; }
        [XmlAttribute(AttributeName = "Version")]
        public string Version { get; set; }
        [XmlAttribute(AttributeName = "PropertyID")]
        public int PropertyID { get; set; }
        [XmlAttribute(AttributeName = "TimeStamp")]
        public string TimeStamp { get; set; }
    }

    [XmlRoot(ElementName = "OTA_HotelResNotifRequest", Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class OTA_HotelResNotifResponse
    {
        [XmlElement(ElementName = "Respuesta", Namespace = "http://www.opentravel.org/OTA/2003/05")]
        public string respuesta { get; set; }
    }
}
