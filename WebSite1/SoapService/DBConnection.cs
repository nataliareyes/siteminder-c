﻿using System;
using System.Collections.Generic;
using log4net;
using MySql.Data.MySqlClient;
using System.Text;
using System.Security.Cryptography;

namespace SoapService
{
    public class DBConnection : IDisposable
    {
        private string connectionString;

        private MySqlConnection connection;

        //Log4Net
        public ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DBConnection(string server, string database, string username, string password)
        {
            this.connectionString = "SERVER=" + server + ";DATABASE=" + database + ";UID=" + username + ";PASSWORD=" + password + ";";

            this.connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                if (connection.State != System.Data.ConnectionState.Open)
                {
                    connection.Close();
                    connection.Open();
                    return true;
                }
                else if (connection.State == System.Data.ConnectionState.Open)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        logger.Error("Cannot connect to server.  Contact administrator - " + ex);
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        logger.Error("Invalid username/password, please try again - " + ex);
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                this.CloseConnection();
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                if (connection.State != System.Data.ConnectionState.Closed)
                {
                    connection.Close();
                    return true;
                }
                return true;
            }
            catch (MySqlException ex)
            {
                this.CloseConnection();
                logger.Error(ex.Message);
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Execute a nonquery (Insert, Update, Delete) that returns the amount of rows affected
        /// </summary>
        /// <param name="query">Query String</param>
        /// <returns>Amount of rows affected</returns>
        private int ExecuteNonQuery(string query)
        {
            try
            {
                if (this.OpenConnection())
                {
                    MySqlCommand command = new MySqlCommand(query, this.connection);

                    int result = command.ExecuteNonQuery();

                    this.CloseConnection();

                    return result;
                }
                else return -1;
            }
            catch (MySqlException e)
            {
                logger.Error(e);
                Console.WriteLine(e);
                this.CloseConnection();
                return -1;
            }
        }

        /// <summary>
        /// Execute a Scalar Query ('Select count(*)')
        /// </summary>
        /// <param name="query">Query String</param>
        /// <returns>Query result</returns>
        private int ExecuteScalarQuery(string query)
        {
            try
            {
                if (this.OpenConnection())
                {
                    MySqlCommand command = new MySqlCommand(query, this.connection);

                    int result = Convert.ToInt32(command.ExecuteScalar());

                    this.CloseConnection();

                    return result;
                }
                else return -1;
            }
            catch (MySqlException e)
            {
                logger.Error(e);
                Console.WriteLine(e);
                this.CloseConnection();
                return -1;
            }
        }

        private List<List<string>> ExecuteSelectQuery(string query)
        {
            try
            {
                if (this.OpenConnection())
                {
                    List<List<string>> result = new List<List<string>>();

                    MySqlCommand command = new MySqlCommand(query, this.connection);

                    MySqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        List<string> row = new List<string>();

                        for (int i = 0; i < dataReader.FieldCount; i++)
                        {
                            string type = dataReader.GetDataTypeName(i);
                            if (type == "TIMESTAMP" || type == "DATETIME" || type == "DATE")
                            {
                                try
                                {
                                    DateTime date = new DateTime();
                                    if (DateTime.TryParse(dataReader.GetMySqlDateTime(i).ToString(), out date)
                                        && dataReader.GetMySqlDateTime(i).ToString() != "00/00/0000 0:00:00")
                                    {
                                        row.Add(date.ToString());
                                    }
                                    else row.Add(null);
                                }
                                catch (Exception e)
                                {
                                    logger.Error(e);
                                    Console.WriteLine(e);
                                    row.Add(null);
                                }
                            }
                            else if (type == "BLOB")
                            {
                                if (dataReader[i] + "" == "")
                                {
                                    row.Add("NULL");
                                }
                                else
                                {
                                    byte[] wii = (byte[])dataReader[i];
                                    int flag = Convert.ToInt32(wii[0]);
                                    if (flag == 48) row.Add("0"); //es lo que viene en Byte para 0
                                    else if (flag == 49) row.Add("1"); //es lo que viene en Byte para 1
                                }
                            }
                            else row.Add(dataReader[i] + "");
                        }

                        result.Add(row);
                    }

                    dataReader.Close();

                    this.CloseConnection();

                    return result;
                }
                else return null;
            }
            catch (MySqlException e)
            {
                logger.Error(e);
                Console.WriteLine(e);
                this.CloseConnection();
                return null;
            }
        }

        /// <summary>
        /// Gets Hotel Name, Check-in time and Check-out time from an hotel's id
        /// </summary>
        /// <param name="idHotel">Hotel's id</param>
        /// <returns>List which contains hotel's name, checkin time, checkout time</returns>
        public List<List<string>> GetNameCheckinCheckoutHotel(int idHotel)
        {
            string query = "select Nombre, checkin_default, checkout_default from mh_mainhoteles " +
                "where id = " + idHotel + " ;";

            return ExecuteSelectQuery(query);
        }

        public List<List<string>> GetDataHotel(int idHotel)
        {
            string query = "select Nombre, checkin_default, checkout_default, flagIconciergePrellegada, flagIconcierge,flagCliente,"
                            + "flagFidelityOnSite,flagFidelity,flagReviewExpress from mh_mainhoteles where id = " + idHotel + " ;";

            return ExecuteSelectQuery(query);
        }

        /// <summary>
        /// Checks if a guest exists, filtering by different alternatives of guest's description, such as name-lastname-roomnumber or
        /// name-lastname-email and name-lastname-checkin checkout time difference.
        /// </summary>
        /// <param name="guest">Guest object with his info</param>
        /// <returns>id of Guest if found, 0 otherwise</returns>
        public int CheckGuestExists(Guest guest)
        {
            string query = "select id from mh_pasajeros where " +
               "id_hotel = " + guest.IdHotel + " and nombre = '" + guest.Name + "' and apellido = '" + guest.LastName + "' and " +
               "checkin between '" + guest.Checkin.AddDays(-3).ToString("yyyy-MM-dd") + "' and '" + guest.Checkin.AddDays(4).ToString("yyyy-MM-dd") + "' " +
               "order by creacion asc ;";

            List<List<string>> result = ExecuteSelectQuery(query);

            if (result != null)
            {
                if (result.Count > 0)
                {
                    int id = Convert.ToInt32(result[result.Count - 1][0]);
                    return id;
                }
                else return 0;
            }
            else return -1;
        }

        public List<List<string>> CheckGuestsByDateRange(int idHotel, DateTime minDateCheckin, DateTime maxDateCheckin)
        {
            string query = "select nombre, apellido, email, habitacion, checkin, checkout, idioma_f, telefono, id_hotel, countryCode, id, flagEmailValido, genero, fecha_nacimiento, idReserva, idPerfil from mh_pasajeros where " +
                   "id_hotel = " + idHotel + " and checkin between '" + minDateCheckin.AddDays(-3).ToString("yyyy-MM-dd") + "' and '" + maxDateCheckin.AddDays(4).ToString("yyyy-MM-dd") + "' " +
                   "order by creacion asc ;";

            List<List<string>> result = ExecuteSelectQuery(query);

            return result;
        }

        /// <summary>
        /// Inserts a guest into the DDBB
        /// </summary>
        /// <param name="guest">Guest object to insert to DDBB</param>
        /// <returns>Numbers of rows affected</returns>
        public int InsertGuest(Guest guest, Hotel hotel)
        {
            string checkin = guest.Checkin.ToString("yyyy-MM-dd") + " " + hotel.CheckinTime.Trim() + ":00";
            string checkout = guest.Checkout.ToString("yyyy-MM-dd") + " " + hotel.CheckoutTime.Trim() + ":00";

            string birthDate = guest.BirthDate == DateTime.MinValue ? "0000-00-00" : guest.BirthDate.ToString("yyyy-MM-dd");

            string query = "insert into mh_pasajeros(id_hotel, nombre, apellido, email, habitacion, password, checkin, checkout, idioma_f, telefono, countryCode, genero, canal_reserva, fecha_nacimiento, idReserva, idPerfil) " +
                " values (" + guest.IdHotel + ", '" + guest.Name + "', '" + guest.LastName + "', '" + guest.Email + "', " +
                "'" + guest.RoomNumber + "', '" + CalculateMD5Hash(guest.RoomNumber) + "', '" + checkin + "', '" + checkout + "', " + guest.Language +
            ", '" + guest.PhoneNumber + "', '" + guest.CountryCode + "', '" + guest.Gender + "', '" + guest.Channel + "', '" + birthDate + "', '" + guest.ReservationId + "', '" + guest.ProfileId + "'); ";

            return ExecuteNonQuery(query);
        }

        /// <summary>
        /// Instead of inserting one by one each guest, we do a bulk query to insert all of them at once
        /// </summary>
        /// <param name="guests">is a Dictionary containing the list of guests and hotel value pairs</param>
        /// <returns>number of rows affected</returns>
        public int InsertBulkGuests(Dictionary<Guest, Hotel> guests)
        {
            if (guests.Count > 0)
            {
                string query = @"insert into mh_pasajeros 
                                        (id_hotel, nombre, apellido, email, habitacion, password, checkin, checkout, idioma_f, telefono, countryCode, genero, canal_reserva, fecha_nacimiento, idReserva, idPerfil)
                                        values ";

                foreach (KeyValuePair<Guest, Hotel> guest in guests)
                {
                    string checkin = guest.Key.Checkin.ToString("yyyy-MM-dd") + " " + guest.Value.CheckinTime.Trim() + ":00";
                    string checkout = guest.Key.Checkout.ToString("yyyy-MM-dd") + " " + guest.Value.CheckoutTime.Trim() + ":00";

                    string birthDate = guest.Key.BirthDate == DateTime.MinValue ? "0000-00-00" : guest.Key.BirthDate.ToString("yyyy-MM-dd");

                    query += "(" + guest.Key.IdHotel + ", '" + guest.Key.Name + "', '" + guest.Key.LastName + "', '" + guest.Key.Email + "', " +
                                "'" + guest.Key.RoomNumber + "', '" + CalculateMD5Hash(guest.Key.RoomNumber) + "', '" + checkin + "', '" + checkout + "', " + guest.Key.Language +
                                ", '" + guest.Key.PhoneNumber + "', '" + guest.Key.CountryCode + "', '" + guest.Key.Gender + "', '" + guest.Key.Channel + "', '" + birthDate + "', '" +
                                guest.Key.ReservationId + "', '" + guest.Key.ProfileId + "'),";
                }

                if (query.EndsWith(",")) query = query.Remove(query.Length - 1) + ";";

                return ExecuteNonQuery(query);
            }
            else return 0;
        }

        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

        public List<List<string>> GetGuestById(int id)
        {
            string query = "select checkin, checkout, email, habitacion from mh_pasajeros where " +
                "id = " + id + " ; ";

            return ExecuteSelectQuery(query);
        }

        public int UpdateGuest(int id, Guest guest, Hotel hotel)
        {
            string checkin = guest.Checkin.ToString("yyy-MM-dd") + " " + hotel.CheckinTime.Trim() + ":00";
            string checkout = guest.Checkout.ToString("yyy-MM-dd") + " " + hotel.CheckoutTime.Trim() + ":00";

            string birthDate = guest.BirthDate == DateTime.MinValue ? "0000-00-00" : guest.BirthDate.ToString("yyyy-MM-dd");

            string query = "update mh_pasajeros set " +
                " nombre = '" + guest.Name + "', apellido = '" + guest.LastName + "', email = '" + guest.Email + "'," +
                " habitacion = '" + guest.RoomNumber + "', password = '" + CalculateMD5Hash(guest.RoomNumber) + "', " +
                " checkin = '" + checkin + "', checkout = '" + checkout + "', idioma_f = " + guest.Language +
                " , telefono = '" + guest.PhoneNumber + "' , countryCode = '" + guest.CountryCode + "', " +
                " genero = '" + guest.Gender + "', canal_reserva = '" + guest.Channel + "', " +
                " fecha_nacimiento = '" + birthDate + "' " +
                "  where id = " + id;

            return ExecuteNonQuery(query);
        }

        public int UpdateGuestsBulk(Dictionary<Guest, Hotel> guests)
        {
            if (guests.Count > 0)
            {
                string query = @"insert into mh_pasajeros 
                                        (id, nombre, apellido, email, habitacion, password, checkin, checkout, idioma_f, telefono, countryCode, genero, canal_reserva, fecha_nacimiento, flagEmailValido, idReserva, idPerfil)
                                        values ";

                foreach (KeyValuePair<Guest, Hotel> guest in guests)
                {
                    if (guest.Key.Id > 0) //por si acaso venga alguno sin Id, o con algun ID raro..
                    {
                        string checkin = guest.Key.Checkin.ToString("yyy-MM-dd") + " " + guest.Value.CheckinTime.Trim() + ":00";
                        string checkout = guest.Key.Checkout.ToString("yyy-MM-dd") + " " + guest.Value.CheckoutTime.Trim() + ":00";

                        string birthDate = guest.Key.BirthDate == DateTime.MinValue ? "0000-00-00" : guest.Key.BirthDate.ToString("yyyy-MM-dd");

                        query += "(" + guest.Key.Id + ", '" + guest.Key.Name + "', '" + guest.Key.LastName + "', '" + guest.Key.Email + "', " +
                                    "'" + guest.Key.RoomNumber + "', '" + CalculateMD5Hash(guest.Key.RoomNumber) + "', '" + checkin + "', '" + checkout + "', " + guest.Key.Language +
                                    ", '" + guest.Key.PhoneNumber + "', '" + guest.Key.CountryCode + "', '" + guest.Key.Gender + "', '"
                                    + guest.Key.Channel + "', '" + birthDate + "', " + guest.Key.FlagEmailValido + ", '" + guest.Key.ReservationId + "', '" + guest.Key.ProfileId + "' ),";
                    }
                }
                if (query.EndsWith(",")) query = query.Remove(query.Length - 1);

                query += @" ON DUPLICATE KEY UPDATE nombre=VALUES(nombre),apellido=VALUES(apellido),email=VALUES(email)
                            ,habitacion=VALUES(habitacion),password=VALUES(password),checkin=VALUES(checkin),checkout=VALUES(checkout)
                            ,idioma_f=VALUES(idioma_f),telefono=VALUES(telefono),countryCode=VALUES(countryCode),genero=VALUES(genero),
                            canal_reserva=VALUES(canal_reserva),fecha_nacimiento=VALUES(fecha_nacimiento),flagEmailValido=VALUES(flagEmailValido),
                            idReserva=VALUES(idReserva),idPerfil=VALUES(idPerfil);";

                return ExecuteNonQuery(query);
            }
            else return 0;
        }

        public int DeleteGuest(int id)
        {
            if (id != 0)
            {
                string query = "delete from mh_pasajeros where id = " + id;
                return ExecuteNonQuery(query);
            }
            else return 0;
        }

        public void QueryInsertIntegrationLog(int idHotel, string status, int countFiles, int countRecord, int countInsert, int countUpdate, DateTime lastModifiedDate, string message)
        {
            try
            {
                string query = "INSERT INTO integrations_log (id_hotel,created_date,integration_status,count_files,count_record,count_insert, count_update, lastModifiedDateFTP, message) " +
                                " VALUES(" + idHotel + ", " + " now() " + ", '" + status + "', " + countFiles + ", " + countRecord + ", " + countInsert + ", "
                                + countUpdate + ", STR_TO_DATE('" + lastModifiedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','%Y-%m-%d %h:%i:%s')" + " ,'" + message + "') ";

                ExecuteNonQuery(query);
            }
            catch (Exception e)
            {
                string log = "Error: " + e.Message + "\nSource: " + e.Source + "\nStack Trace: " + e.StackTrace;
                Console.WriteLine(DateTime.Now.ToString() + " - " + idHotel + " - " + log);
            }
        }

        public int UpdateIntegrationStatus(int hotelId, string status, bool error)
        {
            string checkQuery = "select * from integrations_status where " +
                "idHotel = " + hotelId;

            List<List<string>> result = ExecuteSelectQuery(checkQuery);

            if (result != null)
            {
                if (result.Count > 0)
                {
                    int intError = Convert.ToInt32(error);

                    string query = "update integrations_status set " +
                        "status = '" + status + "' , flagCaida = " + intError + " " +
                        "where idHotel = " + hotelId + " ;";

                    return ExecuteNonQuery(query);
                }
                else
                {
                    int intError = Convert.ToInt32(error);

                    string query = "insert into integrations_status  " +
                        "(idHotel, status, flagCaida) values (" + hotelId + ", '" + status + "' , " + intError + ");";

                    return ExecuteNonQuery(query);
                }
            }
            else
            {
                int intError = Convert.ToInt32(error);

                string query = "insert into integrations_status  " +
                    "(idHotel, status, flagCaida) values (" + hotelId + ", '" + status + "' , " + intError + ");";

                return ExecuteNonQuery(query);
            }
        }

        public List<List<string>> getInfoCountry()
        {
            string query = "SELECT NameEs,Code,Name,Code2, ContinentES, Continent,LanguageToRead,id_fnsrooms  FROM Country ORDER BY ContinentES DESC";

            return ExecuteSelectQuery(query);
        }
        public List<List<string>> GetSTRHotelInfo(int idHotel)
        {
            string query = " select 	str_myhotel_map.idHotelSTR, " +
                           "            str_myhotel_map.strHotelName " +

                           " from 		str_myhotel_map  " +

                           " where		str_myhotel_map.idHotel = " + idHotel;

            return ExecuteSelectQuery(query);
        }

        public int InsertSTRData(STRData strData)
        {
            string date = strData.Date.ToString("yyyy-MM-dd");

            string query = "insert into str_data(idHotel, roomsAvailable, roomsSold, roomsRevenue, dateData, currency) " +
                " values(" + strData.IdHotel + ", " + strData.RoomsAvailable + ", " + strData.RoomsSold + ", " + strData.RoomsRevenue.ToString("0.##") + ", '" + date + "', '" + strData.Currency + "'); ";

            return ExecuteNonQuery(query);
        }

        public List<List<string>> GetLastRecordByHotel(int idHotel)
        {
            string query = "select nombre, email, checkin, checkout, creacion from mh_pasajeros where id_hotel=" + idHotel + " order by creacion desc limit 1;";

            return ExecuteSelectQuery(query);
        }

        public List<List<string>> getListDateLogTodayByHotel(int idHotel)
        {
            string query = "SELECT id_hotel,created_date,integration_status, count_insert, count_update "
                          + " FROM integrations_log WHERE id_hotel=" + idHotel + " and created_date >= CURDATE() ORDER BY created_date desc;";

            return ExecuteSelectQuery(query);
        }

        public List<List<string>> getHotelProfileByHotel(int idHotel)
        {
            string query = " SELECT id_hotel,integration_type,pms,url,repeatingRequest,timeAfterRepeating "
            + " FROM integrations_profile WHERE id_hotel=" + idHotel + ";";

            return ExecuteSelectQuery(query);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}