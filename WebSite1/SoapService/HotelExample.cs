﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapService
{
    public class HotelExample : Hotel
    {
        //Esta es una clase de ejemeplo que usaré al momento de guardar/actualizar los datos.

        public HotelExample(Global.IntegrationType typeIntegration, int idHotel, string PMSName) : base(typeIntegration, idHotel, PMSName)
        {
        }

        public override List<Guest> DoIntegration()
        {
            throw new NotImplementedException();
        }

        protected override DateTime GetDateTimeFromString(string date)
        {
            throw new NotImplementedException();
        }

        protected override string[] GetFormatDate()
        {
            throw new NotImplementedException();
        }
    }
}